import asynctest
import mock
import asyncio
import itertools
from time import time

from aio_hipchat.toolkit import Observable

class TestObservable(asynctest.TestCase):

    def test_events_basic_sync(self):

        observable = Observable('name')
        some_routine = mock.Mock()

        observable.subscribe(lambda: some_routine())

        yield from observable.fire()

        some_routine.assert_any_call()

    def test_events_basic_async(self):

        observable = Observable('name')
        some_routine = mock.Mock()

        @asyncio.coroutine
        def async_coroutine(some_callable):
            some_callable()

        observable.subscribe(async_coroutine)

        yield from observable.fire(some_routine)

        some_routine.assert_any_call()

    def test_events_listeners_execute_in_parallel(self):

        observable = Observable('name')

        @asyncio.coroutine
        def slow_coroutine(cnt):
            yield from asyncio.sleep(1)  # do something expensive
            next(cnt)

        # Register a significant number of slow listeners.
        SWARM=100
        for i in range(SWARM):
            observable.subscribe(slow_coroutine)

        counter = itertools.count()
        start_time = time()
        yield from observable.fire(counter)
        duration = (time() - start_time)

        self.assertEqual(SWARM, next(counter))

        # If the listeners are run in parallel, the total execution will be considerably less than 1 second each
        self.assertLess(duration, SWARM/2, "Workers are not running in parallel")
