import mock
import unittest

from aio_hipchat.config_generator import ConfigGenerator


class TestConfigGenerator(unittest.TestCase):

    def test_precedence(self):

        app_config = {
            'KEY1': 'value1'
        }
        default_config = {
            'KEY1': 'original_value1',
            'KEY2': 'original_value2'
        }
        code_only_config = {
            'KEY1': 'value1_code_only',
            'KEY2': 'value2_code_only',
            'KEY3': 'value3_code_only',
        }

        config_factory = ConfigGenerator(app_config, default_config, code_only_config)
        config = config_factory()

        self.assertEqual(3, len(config))
        self.assertEqual('value1', config['KEY1'])
        self.assertEqual('original_value2', config['KEY2'])
        self.assertEqual('value3_code_only', config['KEY3'])

    def test_missing_parameters_come_from_config(self):

        app_config = {
            'KEY1': 'value1'
        }

        config_factory = ConfigGenerator(app_config)
        config = config_factory()

        # setting comes from config.py default_config
        self.assertEqual(2, config['REDIS_POOL_SIZE'])

        # setting comes from config.py code_only_config
        self.assertEqual('static', config['STATIC_FILES_DIR'])

    def test_boolean_values(self):

        app_config = {
            'TRUE1': True,
            'TRUE2': 1,
            'TRUE3': '1',
            'TRUE4': 'true',
            'TRUE5': 'True',
            'TRUE6': 'TRUE',

            'FALSE1': False,
            'FALSE2': 0,
            'FALSE3': 'anything else',
        }

        # treat all config settings as if they were booleans
        boolean_value_keys = app_config.keys()
        config_factory = ConfigGenerator(app_config, {}, {}, boolean_value_keys)
        config = config_factory()

        self.assertTrue(all(isinstance(x, bool) for x in config.values()))
        self.assertTrue(all(v for (k, v) in config.items() if k.startswith('TRUE')))
        self.assertTrue(all(not v for (k, v) in config.items() if k.startswith('FALSE')))

    def test_environment_vars(self):

        app_config = {
            'KEY1': 'value1'
        }
        environment_vars = {
            'KEY1': "value-from-environment"
        }

        with mock.patch.dict('aio_hipchat.config_generator.os_environ', environment_vars):
            config_factory = ConfigGenerator(app_config, {}, {})
            config = config_factory()

            self.assertEqual('value-from-environment', config['KEY1'])

    def test_environment_vars_dont_override_code_only(self):

        app_config = {
            'KEY1': 'value1'
        }
        code_only = {
            'KEY2': 'value2'
        }
        environment_vars = {
            'KEY2': "value2-from-environment"
        }

        with mock.patch.dict('aio_hipchat.config_generator.os_environ', environment_vars):
            config_factory = ConfigGenerator(app_config, {}, code_only)
            config = config_factory()

            self.assertEqual('value2', config['KEY2'])

    def test_environment_vars_redirection(self):

        app_config = {
            'MONGO_URL': 'value1',
            'MONGO_URL_ALTERNATE_ENV_VAR': 'A_DIFFERENT_ENV_VAR',
        }
        environment_vars = {
            'A_DIFFERENT_ENV_VAR': 'some new value'
        }

        with mock.patch.dict('aio_hipchat.config_generator.os_environ', environment_vars):
            config_factory = ConfigGenerator(app_config, {}, {})
            config = config_factory()

            self.assertEqual('some new value', config['MONGO_URL'])

    def test_environment_vars_redirection_to_non_existent(self):

        app_config = {
            'MONGO_URL': 'value1',
            'MONGO_URL_ALTERNATE_ENV_VAR': 'A_NON_EXISTENT_ENV_VAR',
        }
        environment_vars = {
            'A_DIFFERENT_ENV_VAR': 'some new value'
        }

        with mock.patch.dict('aio_hipchat.config_generator.os_environ', environment_vars):
            try:
                config_factory = ConfigGenerator(app_config, {}, {})
                config = config_factory()
                self.fail('this should have failed to find the redirected env var')
            except ValueError:
                pass  # this is the success case
