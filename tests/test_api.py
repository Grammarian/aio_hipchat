import asyncio
import unittest
from unittest.mock import Mock

from aio_hipchat.api import Api


class ApiImplementationOne(Api):

    @Api.route("/path1", route_name="specific-route-name")
    def method1(self):
        return "first method"

    @Api.route("/path2", ["GET", "PUT"])
    def method2(self):
        return "second method"

class TestApi(unittest.TestCase):

    def test_api_register(self):
        mock_app = Mock()
        my_api = ApiImplementationOne(mock_app)

        self.assertTrue(mock_app.add_route.called)
        self.assertEqual(3, mock_app.add_route.call_count)

        mock_app.add_route.assert_any_call("GET", "/path1", my_api.method1, "specific-route-name", None)
        mock_app.add_route.assert_any_call("GET", "/path2", my_api.method2, "method2", None)
        mock_app.add_route.assert_any_call("PUT", "/path2", my_api.method2, "method2", None)

