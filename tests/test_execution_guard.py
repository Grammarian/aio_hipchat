import asyncio
import asynctest
import mock
import logging

from io import StringIO

import aio_local

from aiohttp import web

from aio_hipchat.execution_guard import ExecutionGuard
from aio_hipchat.log import RequestIdLoggingFilter


class TestExecutionGuard(asynctest.TestCase):

    def test_task_local_logs(self):

        app = mock.Mock()
        app.task_local = aio_local.local()

        logger = logging.getLogger('test')
        for handler in logger.handlers:
            logger.removeHandler(handler)

        # Capture log output to string buffer
        # http://stackoverflow.com/questions/9534245/python-logging-to-stringio-handler
        buffer = StringIO()
        logHandler = logging.StreamHandler(buffer)
        formatter = logging.Formatter("[%(request_path)s#%(request_id)s] [%(name)s] [%(levelname)s] %(message)s")
        logHandler.setFormatter(formatter)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(logHandler)
        logHandler.addFilter(RequestIdLoggingFilter(app))

        guard = ExecutionGuard(logger)

        request = mock.Mock()
        request.headers = {'X-REQUEST-ID': 'SOME_REQ_ID'}
        request.path = '/path/to/request'

        @asyncio.coroutine
        def handler(param_request):
            logger.debug('start')
            asyncio.sleep(.5)
            logger.debug('end')

        result = yield from guard._guard(app, handler, request)

        logHandler.flush()
        log_lines = [x.strip() for x in buffer.getvalue().split('\n') if x]
        for line in log_lines:
            self.assertIn('SOME_REQ_ID', line)
            self.assertIn('/path/to/request', line)


    def test_catches_exceptions(self):

        app = mock.Mock()
        app.task_local = aio_local.local()

        logger = logging.getLogger('test')
        guard = ExecutionGuard(logger)

        request = mock.Mock()

        @asyncio.coroutine
        def handler(param_request):
            raise ValueError('something bad happened')

        result = yield from guard._guard(app, handler, request)

        self.assertIsInstance(result, web.HTTPInternalServerError)
        self.assertIn('something bad happened', result.text)

    def test_allows_http_exceptions(self):

        app = mock.Mock()
        app.task_local = aio_local.local()

        logger = logging.getLogger('test')
        guard = ExecutionGuard(logger)

        request = mock.Mock()

        @asyncio.coroutine
        def handler(param_request):
            raise web.HTTPNotFound()

        try:
            yield from guard._guard(app, handler, request)
            self.fail('this should have raised a NotFound exception')
        except web.HTTPNotFound:
            # success
            pass


