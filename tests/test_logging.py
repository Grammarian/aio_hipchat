import asyncio
import unittest
import asynctest
import logging
from io import StringIO

from aio_hipchat.log import make_default_logger, logged, mlogged

_logger = make_default_logger('test')


def _capture_logs(logger, level):
    # Capture log output to string buffer
    # http://stackoverflow.com/questions/9534245/python-logging-to-stringio-handler
    buffer = StringIO()
    logHandler = logging.StreamHandler(buffer)
    formatter = logging.Formatter("[%(name)s] [%(levelname)s] %(message)s")
    logHandler.setFormatter(formatter)

    logger.setLevel(level)
    logger.addHandler(logHandler)
    return buffer


class TestLogging(unittest.TestCase):

    # noinspection PyUnusedLocal
    @mlogged(dump_args=True)
    def _method_to_log(self, param1, param2, param3, param4):
        _logger.info('do something world changing')
        return 'something incredible'

    # noinspection PyUnusedLocal
    @mlogged(dump_args=False)
    def _method_to_log_no_args(self, param1, param2, param3, param4):
        _logger.info('do something world changing')
        return 'something incredible'

    def test_logged_function(self):

        # noinspection PyUnusedLocal
        @logged(dump_args=True)
        def _function_to_log(param1, param2, param3, param4):
            _logger.info('do something world changing')
            return 'something incredible'

        buffer = _capture_logs(_logger, logging.DEBUG)

        _function_to_log('one', 'two', param3='3', param4=4)

        buffer.flush()
        log_lines = buffer.getvalue().strip().split('\n')
        self.assertEqual("[test] [DEBUG] >>> _function_to_log('one', 'two', param3='3', param4=4)", log_lines[0])
        self.assertIn("[test] [DEBUG] <<< _function_to_log", log_lines[-1])

    def test_logged_method(self):

        buffer = _capture_logs(_logger, logging.DEBUG)

        self._method_to_log('one', 'two', param3='3', param4=4)

        buffer.flush()
        log_lines = buffer.getvalue().strip().split('\n')
        self.assertEqual("[test] [DEBUG] >>> _method_to_log('one', 'two', param3='3', param4=4)", log_lines[0])
        self.assertIn("[test] [DEBUG] <<< _method_to_log", log_lines[-1])

    def test_logged_method_non_debug(self):

        buffer = _capture_logs(_logger, logging.INFO)

        self._method_to_log('one', 'two', param3='3', param4=4)

        buffer.flush()
        log_lines = buffer.getvalue().strip().split('\n')
        self.assertEqual("[test] [INFO] do something world changing", log_lines[0])

    def test_logged_method_without_args(self):

        buffer = _capture_logs(_logger, logging.DEBUG)

        self._method_to_log_no_args('one', 'two', param3='3', param4=4)

        buffer.flush()
        log_lines = buffer.getvalue().strip().split('\n')
        self.assertEqual("[test] [DEBUG] >>> _method_to_log_no_args()", log_lines[0])
        self.assertIn("[test] [DEBUG] <<< _method_to_log_no_args", log_lines[-1])



class TestLoggingAsync(asynctest.TestCase):

    @mlogged(dump_args=True)
    @asyncio.coroutine
    def _coroutine_to_log(self, param1, param2, param3, param4):
        _logger.debug('do something world changing')
        return 'something incredible'

    def test_logged_coroutine(self):

        buffer = _capture_logs(_logger, logging.DEBUG)

        yield from self._coroutine_to_log('one', 'two', param3='3', param4=4)

        buffer.flush()
        log_lines = buffer.getvalue().strip().split('\n')
        self.assertEqual("[test] [DEBUG] >>> _coroutine_to_log('one', 'two', param3='3', param4=4)", log_lines[0])
        self.assertIn("[test] [DEBUG] <<< _coroutine_to_log", log_lines[-1])

