import unittest
import asynctest
import jwt
import mock
import datetime

from aio_hipchat.jwt_auth import JwtAuthenticator


class TestJwtAuth(asynctest.TestCase):

    def test_validate(self):

        app = mock.Mock()

        oauth = mock.Mock()
        oauth.id = 'mock-oauth'
        oauth.secret = 'shhh-its-a-secret'

        app.load_oauth = asynctest.CoroutineMock(return_value=oauth)
        jwt_data = {
            'sub': 'test-sub',
            'context': 'test-context'
        }

        authenticator = JwtAuthenticator(app)
        token = authenticator.create_session_token('issuer', oauth, jwt_data)

        request = mock.Mock()
        request.GET = {}
        request.headers = {'authorization': 'JWT ' + token}

        oauth2, data, signed_request = yield from authenticator.validate(request)

        self.assertIsNotNone(oauth2)
        self.assertIsNotNone(data)
        self.assertIsNotNone(signed_request)

        self.assertEqual(jwt_data['sub'], data['sub'])
        self.assertEqual('issuer', data['iss'])

    def test_validate_invalid_oauth(self):

        app = mock.Mock()
        app.load_oauth = asynctest.CoroutineMock(return_value=None)  # force lookup failure

        oauth = mock.Mock()
        oauth.id = 'mock-oauth'
        oauth.secret = 'shhh-its-a-secret'

        jwt_data = {
            'sub': 'test-sub',
            'context': 'test-context'
        }

        authenticator = JwtAuthenticator(app)
        token = authenticator.create_session_token('issuer', oauth, jwt_data)

        request = mock.Mock()
        request.GET = {}
        request.headers = {'authorization': 'JWT ' + token}

        oauth2, data, signed_request = yield from authenticator.validate(request)

        self.assertIsNone(oauth2)
        self.assertIsNone(data)
        self.assertIsNone(signed_request)

    def test_validate_missing_token(self):

        app = mock.Mock()

        request = mock.Mock()
        request.GET = {}
        request.headers = {}  # no token included

        authenticator = JwtAuthenticator(app)
        oauth2, data, signed_request = yield from authenticator.validate(request)

        self.assertIsNone(oauth2)
        self.assertIsNone(data)
        self.assertIsNone(signed_request)



class TestJwtAuthSync(unittest.TestCase):

    def test_create_session_token(self):

        app = mock.Mock()
        oauth = mock.Mock()
        oauth.id = 'mock-oauth'
        oauth.secret = 'shhh-its-a-secret'
        authenticator = JwtAuthenticator(app)

        jwt_data = {
            'sub': 'test-sub',
            'context': 'test-context'
        }

        token = authenticator.create_session_token('issuer', oauth, jwt_data)
        verified_claims = jwt.decode(token, oauth.secret, audience=oauth.id)

        self.assertEqual('issuer', verified_claims['iss'])
        self.assertEqual(jwt_data['sub'], verified_claims['sub'])
        self.assertEqual(jwt_data['context'], verified_claims['context'])
        self.assertEqual([oauth.id], verified_claims['aud'])

    def test_create_session_token_expired(self):

        app = mock.Mock()
        oauth = mock.Mock()
        oauth.id = 'mock-oauth'
        oauth.secret = 'shhh-its-a-secret'
        authenticator = JwtAuthenticator(app)

        jwt_data = {
            'sub': 'test-sub',
            'context': 'test-context'
        }
        ancient = datetime.datetime(1999, 12, 31, 23, 59, 59, tzinfo=datetime.timezone.utc)
        token = authenticator.create_session_token('issuer', oauth, jwt_data, utc_now=ancient)

        try:
            jwt.decode(token, oauth.secret)
            self.fail('The token should have expired')
        except jwt.exceptions.ExpiredSignatureError:
            pass
