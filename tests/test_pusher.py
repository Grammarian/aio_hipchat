import asynctest
import unittest
import mock
import asyncio
import functools

from aio_hipchat.message_hub import InMemoryMessageHub
from aio_hipchat.pusher import SsePusher, _SseEvent, MultiPusher


class TestSseEvent(unittest.TestCase):

    def test_flatten(self):
        data = {"some": "values"}
        evt = _SseEvent(data, "name", "evt_id")
        flat = evt.flatten("retry_id")
        expected = "event: name\nid: evt_id\ndata: {'some': 'values'}\nretry: retry_id\n\n"
        self.assertEqual(expected, flat)

    def test_flatten_only_non_empty(self):
        data = {"some": "values"}
        evt = _SseEvent(data, "", "")
        flat = evt.flatten("")
        expected = "data: {'some': 'values'}\n\n"
        self.assertEqual(expected, flat)



class TestSsePusher(unittest.TestCase):

    def test_creation_adds_route(self):
        app = mock.Mock()

        pusher = SsePusher(app, '/sse-subscribe')

        app.router.add_route.assert_called_once_with('GET', '/sse-subscribe', pusher._handle_subscription)

    def test_push(self):
        app = mock.Mock()

        pusher = SsePusher(app, '/sse-subscribe')
        q = asyncio.Queue()
        pusher.subscribers.add(q)

        d = {'some': 'value', 'some-other': 'value-other'}
        pusher.push(d)

        evt = q.get_nowait()
        self.assertIsNotNone(evt)

    def test_push_to_non_responsive_queue(self):
        app = mock.Mock()

        pusher = SsePusher(app, '/sse-subscribe')
        q = asyncio.Queue(3)  # only allow 3 items
        pusher.subscribers.add(q)

        d = {'some': 'value', 'some-other': 'value-other'}
        pusher.push(d)
        pusher.push(d)
        pusher.push(d)  # this fills the queue
        pusher.push(d)  # this triggers a dead client

        evt = q.get_nowait()
        self.assertEqual(evt, pusher._time_to_die_sentinel)
        self.assertEqual(0, len(pusher.subscribers))


class TestSsePusherAsync(asynctest.TestCase):

    def test_subscription_starts_pinger(self):
        app = mock.Mock()
        app.loop = asyncio.get_event_loop()

        request = mock.Mock()

        with mock.patch('aio_hipchat.pusher.StreamResponse'):
            pusher = SsePusher(app, '/sse-subscribe', 10)

            # Arrange for the pusher to stop
            app.loop.call_later(1, pusher.stop)

            self.assertIsNone(pusher.pinger)
            _ = yield from pusher._handle_subscription(request)
            self.assertIsNotNone(pusher.pinger)

    def test_pushes_sent_to_stream(self):
        app = mock.Mock()
        app.loop = asyncio.get_event_loop()

        with mock.patch('aio_hipchat.pusher.StreamResponse'):
            pusher = SsePusher(app, '/sse-subscribe', 0, reconnect_interval=12000)

            # Arrange for the pusher to send some data and then stop
            d = {'some': 'data'}
            app.loop.call_later(0.75, functools.partial(pusher.push, d))
            app.loop.call_later(1, pusher.stop)

            # Pretend to be some clients
            futures = [
                pusher._handle_subscription(mock.Mock()),
                pusher._handle_subscription(mock.Mock()),
                pusher._handle_subscription(mock.Mock())
            ]

            responses = yield from asyncio.gather(*futures, loop=app.loop)

            # The event should have been written to each subscriber
            sent = b'data: {"some": "data"}\nretry: 12000\n\n'
            for x in responses:
                x.write.assert_called_with(sent)


class TestMultiPusherAsync(asynctest.TestCase):

    def test_push_forwarded_via_message_hib(self):

        app = mock.Mock()
        app.loop = asyncio.get_event_loop()
        app.message_hub = InMemoryMessageHub()

        pusher = mock.Mock()

        multi_pusher = MultiPusher(app.message_hub, pusher)
        yield from multi_pusher.start()
        d = {'some': 'data'}
        yield from multi_pusher.push(d, 'event_name', 'event_id')

        pusher.push.assert_called_with(data=d, event_id='event_id', event_name='event_name')

