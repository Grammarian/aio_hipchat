import unittest

from aio_hipchat.hc_structures import ImageCard, LinkCard, ApplicationCard, CardAttribute, Glance, GlanceCondition, GlanceContainer

class TestStructures(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None

    def test_glance_container(self):
        glance = Glance("label", lozenge_text='text', lozenge_type='error')
        container = GlanceContainer({"key": glance})
        expected = {
            "glance": [
                {
                    "key": "key",
                    "content": {
                        'label': {
                            'type': 'html',
                            'value': 'label'
                        },
                        'status': {
                            'type': 'lozenge',
                            'value': {
                                'label': 'text',
                                'type': 'error'
                            }
                        }
                    }
                }
            ]
        }
        self.assertDictEqual(expected, container.as_json())

    def test_glance_lozenge(self):
        glance = Glance("label", lozenge_text='text', lozenge_type='error')
        expected = {
            'label': {
                'type': 'html',
                'value': 'label'
            },
            'status': {
                'type': 'lozenge',
                'value': {
                    'label': 'text',
                    'type': 'error'
                }
            }
        }
        self.assertDictEqual(expected, glance.as_json())

    def test_glance_icon(self):
        glance = Glance("label", icon_url='icon_url', icon_url_2x="2x")
        expected = {
            'label': {
                'type': 'html',
                'value': 'label'
            },
            'status': {
                'type': 'icon',
                'value': {
                    'url': 'icon_url',
                    'url@2x': '2x'
                }
            }
        }
        self.assertDictEqual(expected, glance.as_json())

    def test_glance_metadata(self):
        glance = Glance("label", metadata={"some-attr": 2})
        expected = {
            'label': {
                'type': 'html',
                'value': 'label'
            },
            'metadata': {
                'some-attr': 2
            }
        }
        self.assertDictEqual(expected, glance.as_json())

    def test_glance_conditions_simple(self):
        condition = GlanceCondition("room_is_public")
        expected = {
            'conditions': [
                {'condition': 'room_is_public'}
            ]
        }
        self.assertDictEqual(expected, condition.as_json())

    def test_glance_conditions_compound(self):
        condition3 = GlanceCondition(
            "glance_matches", params={"some": "thing"},
            and_=GlanceCondition(
                "not user_is_guest",
                and_=GlanceCondition("room_is_public")
            )
        )
        expected = {
            'conditions': [
                {
                    'condition': 'glance_matches',
                    'params': {
                        'metadata': [
                            {'some': 'thing'}
                        ]
                    }
                },
                {
                    'conditions': [
                        {
                            'condition': 'user_is_guest',
                            'invert': True
                        },
                        {
                            'conditions': [
                                {
                                    'condition': 'room_is_public'
                                }
                            ]
                        }
                    ],
                    'type': 'and'
                }
            ],
            'type': 'and'
        }
        self.assertDictEqual(expected, condition3.as_json())

    def test_link_card(self):

        card = LinkCard(title='title', description='description', description_is_html=False,
                        link_url='link_url', icon_url='icon_url', icon_url_2x='icon_url_2x', id='some unique id')
        expected = {
            'description': 'description',
            'id': 'some unique id',
            'style': 'link',
            'icon': {
                'url': 'icon_url',
                'url@2x': 'icon_url_2x',
            },
            'title': 'title',
            'url': 'link_url'
        }

        self.assertDictEqual(expected, card.as_json())

    def test_activity_card(self):

        self.maxDiff = None
        card = ApplicationCard(
            title='title', description='description', description_is_html=False,
            link_url='link_url', icon_url='icon_url', icon_url_2x='icon_url_2x', id='some unique id',
            attributes=[
                CardAttribute('label', link_url='link_url', icon_url='icon_url', icon_url_2x='icon_url_2x',
                              second_label='second_label', second_label_style='lozenge-complete'),
                CardAttribute('label2', link_url='link_url2', icon_url='icon_url2', icon_url_2x='icon_url_2x2',
                              second_label='second_label2', second_label_style='lozenge-error')
            ]
        )
        expected = {
            'attributes': [
                {
                    'label': 'label',
                    'value': {
                        'icon': {
                            'url': 'icon_url',
                            'url@2x': 'icon_url_2x'
                        },
                        'label': 'second_label',
                        'style': 'lozenge-complete',
                        'url': 'link_url'
                    }
                },
                {
                    'label': 'label2',
                    'value': {
                        'icon': {
                            'url': 'icon_url2',
                            'url@2x': 'icon_url_2x2'
                        },
                        'label': 'second_label2',
                        'style': 'lozenge-error',
                        'url': 'link_url2'
                    }
                }
            ],
            'format': 'compact',
            'description': 'description',
            'id': 'some unique id',
            'style': 'application',
            'icon': {
                'url': 'icon_url',
                'url@2x': 'icon_url_2x',
            },
            'title': 'title',
            'url': 'link_url'
        }

        self.assertDictEqual(expected, card.as_json())

    def test_image_card(self):

        card = ImageCard('image_url', title='title', description='description', description_is_html=False,
                         link_url='link_url', image_url_2x='image_url_2x', width=100, height=200, id='some unique id',
                         side_by_side=False)
        expected = {
            'description': 'description',
            'id': 'some unique id',
            'style': 'image',
            'thumbnail': {
                'height': 200,
                'url': 'image_url',
                'url@2x': 'image_url_2x',
                'width': 100
            },
            'title': 'title',
            'url': 'link_url'
        }

        self.assertDictEqual(expected, card.as_json())

    def test_image_card_side_by_side(self):

        card = ImageCard('image_url', title='title', description='description', description_is_html=True,
                         link_url='link_url', image_url_2x='image_url_2x', width=100, height=200, id='some unique id',
                         side_by_side=True)
        expected = {
            'description': 'description',
            'id': 'some unique id',
            'style': 'media',
            'thumbnail': {
                'height': 200,
                'url': 'image_url',
                'url@2x': 'image_url_2x',
                'width': 100
            },
            'title': 'title',
            'url': 'link_url'
        }

        self.assertDictEqual(expected, card.as_json())
