import asyncio
import json
import unittest
import asynctest
import aio_hipchat.toolkit as tk
from aiohttp import web

_CORS_HEADER = "Access-Control-Allow-Origin"


class TestToolkit(unittest.TestCase):

    def test_cors(self):
        resp = web.Response(text='something', content_type=tk.CONTENT_TYPE_TEXT)
        self.assertNotIn(_CORS_HEADER, resp.headers)
        tk.add_allow_cross_origin(resp)
        self.assertIn(_CORS_HEADER, resp.headers)


class TestToolkitAsync(asynctest.TestCase):

    def test_maybe_coroutine_func(self):

        def sync_func(n):
            return 'the answer is {}'.format(n)

        resp = yield from tk.maybe_coroutine(sync_func, 42)
        self.assertEquals('the answer is 42', resp)

    def test_maybe_coroutine_coroutine(self):

        @asyncio.coroutine
        def async_func(n):
            asyncio.sleep(1)
            return 'the answer is {}'.format(n)

        resp = yield from tk.maybe_coroutine(async_func, 42)
        self.assertEquals('the answer is 42', resp)

    def test_cors_decorator_func(self):

        @tk.allow_cross_origin
        def response_getter():
            return web.Response(text='something', content_type=tk.CONTENT_TYPE_TEXT)

        resp = yield from response_getter()
        self.assertIn(_CORS_HEADER, resp.headers)

    def test_cors_decorator_coroutine(self):

        @tk.allow_cross_origin
        @asyncio.coroutine
        def response_getter():
            return web.Response(text='something', content_type=tk.CONTENT_TYPE_TEXT)

        resp = yield from response_getter()
        self.assertIn(_CORS_HEADER, resp.headers)


class TestToolkitSimpleResponse(unittest.TestCase):

    def test_none(self):
        result = tk.simple_response(None)
        self.assertIsInstance(result, web.HTTPNoContent)

    def test_existing_response(self):
        resp = web.Response(text='something', content_type=tk.CONTENT_TYPE_TEXT)
        result = tk.simple_response(resp)
        self.assertIs(result, resp)

    def test_structure_dict(self):
        input = {'A': 1, 'B': 2, 'C': {'D': 4}}
        result = tk.simple_response(input)
        self.assertIsInstance(result, web.Response)
        self.assertDictEqual(input, json.loads(result.text))
        self.assertEqual(tk.CONTENT_TYPE_JSON, result.content_type)

    def test_structure_list(self):
        input = ['A', 1, 'B', 2, 'C', {'D': 4}]
        result = tk.simple_response(input)
        self.assertIsInstance(result, web.Response)
        self.assertEqual(input, json.loads(result.text))
        self.assertEqual(tk.CONTENT_TYPE_JSON, result.content_type)

    def test_string(self):
        input = 'some boring input'
        result = tk.simple_response(input)
        self.assertIsInstance(result, web.Response)
        self.assertEqual(input, result.text)
        self.assertEqual(tk.CONTENT_TYPE_TEXT, result.content_type)

    class _SomeModelClass:
        def as_json(self):
            return {'some': 'json'}

    def test_as_json(self):
        input = self._SomeModelClass()
        result = tk.simple_response(input)
        self.assertIsInstance(result, web.Response)
        self.assertDictEqual(input.as_json(), json.loads(result.text))
        self.assertEqual(tk.CONTENT_TYPE_JSON, result.content_type)
