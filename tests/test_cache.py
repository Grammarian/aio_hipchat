import asyncio
import asynctest

from aio_hipchat.cache import InMemoryCache


class TestInMemoryCache(asynctest.TestCase):

    def test_set_ex(self):
        cache = InMemoryCache()
        yield from cache.setex('KEY', 30, 'value')
        value = yield from cache.get('KEY')
        self.assertEqual('value', value)

    def test_set_ex_expires(self):
        cache = InMemoryCache()
        yield from cache.setex('KEY', 1, 'value')
        yield from asyncio.sleep(2)
        value = yield from cache.get('KEY')
        self.assertIsNone(value)

    def test_expires(self):
        cache = InMemoryCache()
        yield from cache.setex('KEY', 1, 'value')
        self.assertFalse(cache._is_expired('KEY'))
        yield from asyncio.sleep(2)
        self.assertTrue(cache._is_expired('KEY'))

    def test_sadd(self):
        cache = InMemoryCache()
        yield from cache.sadd('KEY', ['1'])
        yield from cache.sadd('KEY', ['1', '2', '3'])
        s = yield from cache.smembers('KEY')
        self.assertIn('1', s)
        self.assertIn('2', s)
        self.assertIn('3', s)

    def test_exists(self):
        cache = InMemoryCache()
        yield from cache.sadd('KEY', ['1', '2', '3'])
        b = yield from cache.exists('KEY')
        self.assertTrue(b)
        
    def test_delete(self):
        cache = InMemoryCache()
        yield from cache.set('KEY', 'value')
        yield from cache.delete('KEY')
        b = yield from cache.exists('KEY')
        self.assertFalse(b)

    def test_srem(self):
        cache = InMemoryCache()
        yield from cache.sadd('KEY', ['1', '2', '3'])
        yield from cache.srem('KEY', ['3'])
        s = yield from cache.smembers('KEY')
        self.assertNotIn('3', s)

    def test_incr(self):
        cache = InMemoryCache()
        yield from cache.set('KEY', 1999)
        yield from cache.incr('KEY')
        v = yield from cache.get('KEY')
        self.assertEqual(2000, v)
        
    def test_incr_by(self):
        cache = InMemoryCache()
        yield from cache.set('KEY', 1999)
        yield from cache.incr('KEY', 1000)
        v = yield from cache.get('KEY')
        self.assertEqual(2999, v)

    def test_decr(self):
        cache = InMemoryCache()
        yield from cache.set('KEY', 1999)
        yield from cache.decr('KEY')
        v = yield from cache.get('KEY')
        self.assertEqual(1998, v)

    def test_list(self):
        cache = InMemoryCache()
        yield from cache.lpush('KEY', ['1', '2', '3'])
        yield from cache.lpush('KEY', ['3', '4', '5'])
        yield from cache.lpush('KEY', ['5', '6', '7'])
        lst = yield from cache.lrange('KEY')
        self.assertEqual(9, len(lst))
        self.assertIn('1', lst)
        self.assertIn('7', lst)
