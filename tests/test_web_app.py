import asyncio
import unittest

import asynctest
import mock
from aio_hipchat.web_app import WebApp

default_app_config = {
    'APP_NAME': 'unit-testing-app',
    'BASE_URL': 'https://somewhere/with/path',
    'JINJA_VIEWS_DIR': '',
    'STATIC_FILES_DIR': '',
}


class TestWebAppSync(unittest.TestCase):

    def test_standard_handlers_installed(self):

        config = default_app_config.copy()
        config['USE_STANDARD_HANDLERS'] = True
        app = WebApp(__file__, config)
        self.assertIn('healthcheck', app.router)

    def test_maybe_relative_to_base(self):
        config = default_app_config.copy()
        config['BASE_URL'] = 'http://base/path'

        app = WebApp(__file__, config)

        self.assertEqual('http://base/path/relative', app.maybe_relative_to_base('relative'))
        self.assertEqual('http://absolute/path', app.maybe_relative_to_base('http://absolute/path'))

    def test_relative_to_base(self):

        config = default_app_config.copy()
        config['BASE_URL'] = 'http://base/path'

        app = WebApp(__file__, config)

        self.assertEqual('http://base/path/relative', app.relative_to_base('relative'))
        self.assertEqual('http://base/path/relative', app.relative_to_base('/relative'))

    def test_relative_to_base_trailing_slash(self):

        config = default_app_config.copy()
        config['BASE_URL'] = 'http://base/path/'

        app = WebApp(__file__, config)

        self.assertEqual('http://base/path/relative', app.relative_to_base('relative'))
        self.assertEqual('http://base/path/relative', app.relative_to_base('/relative'))

    def test_add_route_responds_without_trailing_slash(self):

        def handler(request):
            pass

        app = WebApp(__file__, default_app_config.copy())
        app.add_route('GET', '/path/', handler, "route", None)

        self.assertTrue(any(x.match("/path") is not None for x in app.router.routes()))
        self.assertTrue(any(x.match("/path/") is not None for x in app.router.routes()))


class TestWebAppAsync(asynctest.TestCase):

    def test_events_started(self):

        some_routine = mock.Mock()

        app = WebApp(__file__, default_app_config.copy())
        app.started.subscribe(lambda: some_routine())

        # give the events a chance to execute
        yield from asyncio.sleep(1)
        some_routine.assert_any_call()

