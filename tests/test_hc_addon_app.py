import asyncio
import asynctest
import json
import mock
import unittest
from aiohttp import web

from aio_hipchat.hc_addon_app import HcAddonApp
from aio_hipchat.hc_addon_api import HcAddonApi
from aio_hipchat.toolkit import dict_without


default_app_config = {
    'ADDON_KEY': 'add-on-key',
    'ADDON_NAME': 'Addon Name',
    'ADDON_DESCRIPTION': 'Addon description',
    'ADDON_VENDOR_NAME': 'Vendor',
    'ADDON_VENDOR_URL': 'Vendor url',
    'ADDON_FROM_NAME': 'Source from name',
    'ADDON_AVATAR': 'http://absolute-url-to-avatar',
    'ADDON_AVATAR_2X': '/relative-url-to-hires-avatar',
    'ADDON_ALLOW_ROOM': 'true',
    'ADDON_ALLOW_GLOBAL': 'false',
    'ADDON_SCOPES': 'scope1 scopes2',  # space-separated list of scopes required by the add on
    'APP_NAME': 'testing-app',
    'BASE_URL': 'https://somewhere/with/path',
    'JINJA_VIEWS_DIR': '',
    'STATIC_FILES_DIR': '',
}


class TestHcAddonAppSync(unittest.TestCase):

    def test_routes_registered_on_creation(self):

        app = HcAddonApp(__file__, default_app_config.copy())

        self.assertTrue(any(x.match("/") is not None for x in app.router.routes()))
        self.assertTrue(any(x.match("/installable") is not None for x in app.router.routes()))
        self.assertTrue(any(x.match("/installable/SOMEOAUTHID") is not None for x in app.router.routes()))

    def test_config_validation_required_fields(self):

        required_fields = ['APP_NAME', 'ADDON_NAME', 'ADDON_SCOPES', 'BASE_URL']
        for required in required_fields:
            try:
                config = dict_without(default_app_config, required)
                addon = HcAddonApp(__file__, config)
                self.fail('Should have thrown invalid config exception for ' + required)
            except ValueError:
                # This is the success case
                continue

    def test_config_validation_non_empty_fields(self):

        required_fields = ['APP_NAME', 'ADDON_NAME', 'ADDON_SCOPES', 'BASE_URL']
        for required in required_fields:
            try:
                config = default_app_config.copy()
                config[required] = ''
                addon = HcAddonApp(__file__, config)
                self.fail('Should have thrown invalid config exception for ' + required)
            except ValueError:
                # This is the success case
                continue

    def test_install_validation_non_empty_fields(self):

        install_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'capabilitiesUrl': 'linktoself'
        }

        required_fields = ['oauthId', 'oauthSecret', 'capabilitiesUrl']
        for required in required_fields:
            addon = HcAddonApp(__file__, default_app_config.copy())
            expurgated = dict_without(install_json, required)
            err = addon._validate_install_request(expurgated, {})
            self.assertIsNotNone(err)

    def test_install_validation_global_addon_in_room(self):

        install_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'capabilitiesUrl': 'linktoself'
        }

        config = default_app_config.copy()
        config['ADDON_ALLOW_ROOM'] = 'False'
        addon = HcAddonApp(__file__, config)
        err = addon._validate_install_request(install_json, {})
        self.assertIsNotNone(err)

    def test_install_validation_room_addon_in_global(self):

        install_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'capabilitiesUrl': 'linktoself'
        }
        # lack of roomId in the above means the install is trying to be global

        config = default_app_config.copy()
        config['ADDON_ALLOW_GLOBAL'] = 'False'
        addon = HcAddonApp(__file__, config)
        err = addon._validate_install_request(install_json, {})
        self.assertIsNotNone(err)

    def test_install_validation_unexpected_location(self):

        install_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'capabilitiesUrl': 'linktoself'
        }

        capabilities_json = {
            'links': {
                'self': 'DIFFERENT',
            },
        }

        addon = HcAddonApp(__file__, default_app_config.copy())
        err = addon._validate_install_request(install_json, capabilities_json)
        self.assertEqual("The capabilities URL 'linktoself' doesn't match the resource's self link 'DIFFERENT'", err)


class TestHcAddon(asynctest.TestCase):

    def setUp(self):
        self.maxDiff = None

    def test_get_descriptor_from_config(self):

        app_config = {
            'APP_NAME': 'SomeApp',
            'ADDON_KEY': 'add-on-key',
            'ADDON_NAME': 'Addon Name',
            'ADDON_DESCRIPTION': 'Addon description',
            'ADDON_VENDOR_NAME': 'Vendor',
            'ADDON_VENDOR_URL': 'Vendor url',
            'ADDON_FROM_NAME': 'Source from name',
            'ADDON_AVATAR': 'http://absolute-url-to-avatar',
            'ADDON_AVATAR_2X': '/relative-url-to-hires-avatar',
            'ADDON_ALLOW_ROOM': 'true',
            'ADDON_ALLOW_GLOBAL': 'false',
            'ADDON_SCOPES': 'scope1 scopes2',  # space-separated list of scopes required by the add on
            'BASE_URL': 'https://somewhere/with/path',
            'JINJA_VIEWS_DIR': '',
            'STATIC_FILES_DIR': '',
        }
        addon = HcAddonApp(__file__, app_config)

        request = mock.Mock()
        response = yield from addon.get_descriptor(request)

        expected = (
            '{"capabilities": {"hipchatApiConsumer": {'
            '"avatar": {"url": "http://absolute-url-to-avatar", '
            '"url@2x": "https://somewhere/with/path/relative-url-to-hires-avatar"}, '
            '"fromName": "Source from name", '
            '"scopes": ["scope1", "scopes2"]}, '
            '"installable": {"allowGlobal": "false", "allowRoom": "true", '
            '"callbackUrl": "https://somewhere/with/path/installable/eyJzY29wZXMiOiBbInNjb3BlMSIsICJzY29wZXMyIl19"}}, '
            '"description": "Addon description", '
            '"key": "add-on-key", '
            '"links": {"self": "https://somewhere/with/path"}, '
            '"name": "Addon Name", '
            '"vendor": {"name": "Vendor", "url": "Vendor url"}}'
        )

        self.assertIsInstance(response, web.Response)
        sorted_response = json.dumps(json.loads(response.text), sort_keys=True)
        # print(sorted_response)
        self.assertEqual(expected, sorted_response)

    def test_on_install(self):

        request = mock.Mock()
        request.match_info = {}
        request_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'capabilitiesUrl': 'linktoself'
        }
        request.json = asynctest.CoroutineMock(return_value=request_json)

        capabilities_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'links': {
                'self': 'linktoself',
                'homepage': 'link-to-home-page'
            },
            'capabilities': {
                'oauth2Provider' : {
                    'tokenUrl': 'url-for-tokens'
                }
            }
        }

        token_json = {
            'group_id': '97',
            'group_name': 'test group',
            'access_token': 'SOMETOKEN',
            'expires_in': 120,
        }

        mock_retry = asynctest.CoroutineMock(return_value=capabilities_json)
        mock_retry_json_ex = asynctest.CoroutineMock(return_value=(200, token_json, ''))
        with mock.patch('aio_hipchat.toolkit.retry_json', mock_retry):
            with mock.patch('aio_hipchat.toolkit.retry_json_ex', mock_retry_json_ex):
                addon = HcAddonApp(__file__, default_app_config.copy())
                response = yield from addon.on_install(request)

        expected = {
            'capabilities_url': 'linktoself',
            'group_id': '97',
            'group_name': 'test group',
            'homepage': 'link-to-home-page',
            'id': 'someOAuthId',
            'room_id': '987',
            'scopes': 'scope1,scopes2',
            'secret': 'someOAuthSecret',
            'token_url': 'url-for-tokens'
        }

        self.assertIsInstance(response, web.HTTPCreated)
        oauth = yield from addon.store.get('someOAuthId')
        self.assertDictEqual(expected, oauth)

    def test_on_install_with_group_validator(self):

        request = mock.Mock()
        request.match_info = {}
        request_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'capabilitiesUrl': 'linktoself'
        }
        request.json = asynctest.CoroutineMock(return_value=request_json)

        capabilities_json = {
            'oauthId': 'someOAuthId',
            'oauthSecret': 'someOAuthSecret',
            'roomId': '987',
            'links': {
                'self': 'linktoself',
                'homepage': 'link-to-home-page'
            },
            'capabilities': {
                'oauth2Provider' : {
                    'tokenUrl': 'url-for-tokens'
                }
            }
        }

        msg = "I don't care what the value is, there is no way I'm going to let you install this"

        def validator(groupId):
            return msg

        with mock.patch('aio_hipchat.toolkit.retry_json', asynctest.CoroutineMock(return_value=capabilities_json)):
            with mock.patch('aio_hipchat.hc_addon_app.OAuth') as mock_installation:
                mock_installation.return_value.group_id = '97'
                addon = HcAddonApp(__file__, default_app_config.copy(), group_validator=validator)
                response = yield from addon.on_install(request)

        self.assertIsInstance(response, web.HTTPBadRequest)
        self.assertEqual(msg, response.reason)

    class _MyTestHooksApi(HcAddonApi):
        api = HcAddonApi

        @api.message_hook('/command ', name='first-command', auth='none')
        def handle_slash_command(self, request):
            pass

        @api.message_hook('/second ', name='second-handler')
        def handle_second_slash_command(self, request):
            pass

        @api.webhook('room_unarchived', name='unarchive-handler')
        def handle_room_unarchive(self, request):
            pass

    def test_webhook(self):

        app = HcAddonApp(__file__, default_app_config.copy())
        api = self._MyTestHooksApi(app)
        descriptor = yield from app._generate_descriptor()

        expected = {
            'capabilities': {
                'hipchatApiConsumer': {
                    'avatar': {
                        'url': 'http://absolute-url-to-avatar',
                        'url@2x': 'https://somewhere/with/path/relative-url-to-hires-avatar'
                    },
                    'fromName': 'Source from name',
                    'scopes': ['scope1', 'scopes2']
                },
                'installable': {
                    'allowGlobal': 'false',
                    'allowRoom': 'true',
                    'callbackUrl': 'https://somewhere/with/path/installable/eyJzY29wZXMiOiBbInNjb3BlMSIsICJzY29wZXMyIl19'
                },
                'webhook': [
                    {'authentication': 'jwt',
                     'event': 'room_unarchived',
                     'name': 'unarchive-handler',
                     'url': 'https://somewhere/with/path/event/room_unarchived/unarchive-handler'},
                    {'authentication': 'jwt',
                     'event': 'room_message',
                     'name': 'second-handler',
                     'pattern': '/second ',
                     'url': 'https://somewhere/with/path/event/room_message/second-handler'},
                    {'authentication': 'none',
                     'event': 'room_message',
                     'name': 'first-command',
                     'pattern': '/command ',
                     'url': 'https://somewhere/with/path/event/room_message/first-command'},
                ]
            },
            'description': 'Addon description',
            'key': 'add-on-key',
            'links': {
                'self': 'https://somewhere/with/path'
            },
            'name': 'Addon Name',
            'vendor': {
                'name': 'Vendor',
                'url': 'Vendor url'
            }
        }

        self.assertDictEqual(expected, descriptor)

    class _MyTestGlanceApi(HcAddonApi):
        api = HcAddonApi

        @api.glance('test.glance', label='glance-label', icon='some-icon')
        def handle_slash_command(self, request):
            pass

    def test_glance(self):
        app = HcAddonApp(__file__, default_app_config.copy())
        api = self._MyTestGlanceApi(app)
        descriptor = yield from app._generate_descriptor()

        expected = {
            'glance': [
                {
                    'icon': {
                        'url': 'https://somewhere/with/path/some-icon',
                        'url@2x': 'https://somewhere/with/path/some-icon'
                    },
                    'key': 'test.glance',
                    'name': {'value': 'glance-label'},
                    'queryUrl': 'https://somewhere/with/path/glance/test.glance',
                    'target': 'test.glance.sidebar'
                }
            ],
            'hipchatApiConsumer': {
                'avatar': {
                    'url': 'http://absolute-url-to-avatar',
                    'url@2x': 'https://somewhere/with/path/relative-url-to-hires-avatar'
                },
                'fromName': 'Source from name',
                'scopes': ['scope1', 'scopes2']
            },
            'installable': {
                'allowGlobal': 'false',
                'allowRoom': 'true',
                'callbackUrl': 'https://somewhere/with/path/installable/eyJzY29wZXMiOiBbInNjb3BlMSIsICJzY29wZXMyIl19'
            }
        }

        self.assertDictEqual(expected, descriptor['capabilities'])

    class _MyTestWebPanelApi(HcAddonApi):
        api = HcAddonApi

        @api.webpanel('test.webpanel', 'Label for webpanel')
        def handle_web_panel(self, request):
            pass

    def test_webpanel(self):
        app = HcAddonApp(__file__, default_app_config.copy())
        api = self._MyTestWebPanelApi(app)
        descriptor = yield from app._generate_descriptor()

        expected = {
            'hipchatApiConsumer': {
                'avatar': {
                    'url': 'http://absolute-url-to-avatar',
                    'url@2x': 'https://somewhere/with/path/relative-url-to-hires-avatar'}
                ,
                'fromName': 'Source from name',
                'scopes': ['scope1', 'scopes2']
            },
            'installable': {
                'allowGlobal': 'false',
                'allowRoom': 'true',
                'callbackUrl': 'https://somewhere/with/path/installable/eyJzY29wZXMiOiBbInNjb3BlMSIsICJzY29wZXMyIl19'
            },
            'webPanel': [
                {
                    'icon': {
                        'url': 'https://somewhere/with/path/',
                        'url@2x': 'https://somewhere/with/path/'
                    },
                    'key': 'test.webpanel',
                    'location': 'hipchat.sidebar.right',
                    'name': {'value': 'Label for webpanel'},
                    'url': 'https://somewhere/with/path/webpanel/test.webpanel'
                }
            ]
        }

        self.assertDictEqual(expected, descriptor['capabilities'])

    class _MyEmptyApi(HcAddonApi):
        pass

        @asyncio.coroutine
        def some_method(self, request):
            if 0: yield
            return {'result': 'success'}

    def test_wrapping_auth(self):
        app = mock.Mock()
        oauth = mock.Mock()
        oauth.id = 'oauth_id'
        jwt_data = mock.Mock()
        signed_request = mock.Mock()
        app.authenticator.validate = asynctest.CoroutineMock(return_value=(oauth, jwt_data, signed_request))
        app.authenticator.create_session_token = mock.Mock(return_value='TOKEN')

        api = self._MyEmptyApi(app)
        wrapped = api._wrap_method(api.some_method, require_auth=True, allow_cors=True, extract=False)

        request = mock.Mock()
        request.GET = {'theme': 'something'}
        resp = yield from wrapped(request)

        self.assertIsNotNone(resp)
        self.assertIsInstance(resp, web.Response)
        self.assertEqual('{"result": "success"}', resp.text)
        self.assertIn('Access-Control-Allow-Origin', resp.headers)

        self.assertEqual('something', request.theme)
        self.assertEqual('something', request.theme)
        self.assertEqual(oauth, request.oauth)
        self.assertEqual('oauth_id', request.oauth_client_id)
        self.assertEqual('TOKEN', request.token)

    def test_wrapping_auth_failed(self):
        app = mock.Mock()
        app.authenticator.validate = asynctest.CoroutineMock(return_value=(None, None, None))
        api = self._MyEmptyApi(app)

        wrapped = api._wrap_method(api.some_method, require_auth=True, allow_cors=False, extract=False)
        request = mock.Mock()
        resp = yield from wrapped(request)

        self.assertIsNotNone(resp)
        self.assertIsInstance(resp, web.HTTPUnauthorized)

    def test_extract_details(self):

        app = mock.Mock()
        oauth = mock.Mock()
        app.load_oauth = asynctest.CoroutineMock(return_value=oauth)
        api = self._MyEmptyApi(app)

        data = {
            'oauth_client_id': 'ID',
            'item': {
                'message': {'a': 1},
                'room': {'b': 2},
                'sender': {'c': 3},
            }
        }
        request = mock.MagicMock()
        request.__iter__.return_value = []
        request.json = asynctest.CoroutineMock(return_value=data)

        yield from api._extract_details(request)

        self.assertDictEqual(data, request.body_as_json)
        self.assertEqual(oauth, request.oauth)
        self.assertIn('a', request.message)
        self.assertIn('b', request.room)
        self.assertIn('c', request.sender)
