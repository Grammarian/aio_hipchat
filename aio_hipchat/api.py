import inspect
import logging

_logger = logging.getLogger(__name__)


class Api:
    """
    Base class for all externally visible api classes.

    Classes that provide the external api for this web application should subclass this
    and then decorate some methods with @route
    """

    _api_marker = "_api_marker_"

    def __init__(self, app):
        self.app = app
        self._process_api()

    @staticmethod
    def route(path, http_methods=None, route_name=None, expect_handler=None):
        """
        Mark a given method as being part of the application's interface

        path may be either constant string like '/a/b/c' or variable rule like '/a/{var}'

        :param path:route path. Should be started with slash ('/').
        :param http_methods: a collection of HTTP method for route. Should be one or more of
        'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'OPTIONS' or '*' for any method.
        'GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'OPTIONS' or '*' for any method.
        Defaults to ['GET']. Case insensitive.
        :param route_name: (str) – optional route name. Use to dereference the route
        :param expect_handler: (coroutine) – optional expect header handler.
        """

        def inner(f):
            Api._mark_func_as_api(f, path, http_methods, route_name, expect_handler)
            return f

        return inner

    @staticmethod
    def _mark_func_as_api(func, path, http_methods=None, route_name=None, expect_handler=None, args=None):
        """
        This is the non-decorator version of @route
        """

        # Make sure http_methods is a collection
        if http_methods:
            if isinstance(http_methods, str):
                http_methods = [http_methods]
        else:
            http_methods = ["GET"]

        parameters = (path, http_methods, route_name or func.__name__, expect_handler, args)
        setattr(func, Api._api_marker, parameters)

    def _process_api(self):
        """
        Collect the methods of this class that are marked as api methods,
        and register them with the web application's router
        """
        for (name, method) in self._get_api_methods():
            parameters = getattr(method, Api._api_marker)
            self._register_api(name, method, *parameters)

    def _get_api_methods(self):
        """
        Returns the methods of this object with have been marked with the @route decorator
        """
        for (name, method) in inspect.getmembers(self, inspect.ismethod):
            if hasattr(method, Api._api_marker):
                yield (name, method)

    def _register_api(self, method_name, method, path, http_methods, route_name, expect_handler, args):
        """
        Register the given method for the given path in our app.
        """
        _logger.info("Registering handler for %s %s: %s", http_methods, path, method_name)
        for http_method in http_methods:
            self.app.add_route(http_method, path, method, route_name, expect_handler)


