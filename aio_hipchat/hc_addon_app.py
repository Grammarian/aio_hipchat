import asyncio
import binascii
import json
import logging

import aiohttp
import base64
from aiohttp import web

from .oauth import OAuth
from .jwt_auth import JwtAuthenticator
from .toolkit import nested_get, dict_without, Observable
from .web_app import WebApp

_logger = logging.getLogger(__name__)


@asyncio.coroutine
def __is_request_from_hipchat_server(request):

    # What is this all about? It's taken from the original code, but
    # it seems fragile -- if the server is slow, the timeout will fire
    # and the request will be treated as if its *not* from hipchat server

    hipchat_server = True
    forwarded_for = request.headers.get('X-FORWARDED-FOR')
    is_ngrok = request.headers['HOST'] and 'ngrok' in request.headers['HOST']
    if forwarded_for and not is_ngrok:
        try:
            capabilities_url = 'http://{ip}/v2/capabilities'.format(ip=forwarded_for)
            resp = (yield from asyncio.wait_for(aiohttp.request('GET', capabilities_url), timeout=1))
            capdoc = yield from resp.read(decode=True)
            hipchat_server = capdoc['links']['self'] == 'https://api.hipchat.com/v2/capabilities'
        except:
            hipchat_server = False

    return hipchat_server

INSTALLABLE_PATH = '/installable'


def _invalid_install(message):
    _logger.error('OAuth failed: %s' % message)
    return web.HTTPBadRequest(reason=message)


class HcAddonApp(WebApp):
    """
    Declares a HC addon application.

    This class handles installation, and descriptor generation.
    """

    def __init__(self, source_file, config, *args, scopes_getter=None, group_validator=None, **kwargs):
        self.scopes_getter = scopes_getter  # allow scopes to be calculated dynamically
        self.group_validator = group_validator  # allow installations to particular groups
        self.capabilities_descriptions = {}

        self.installed = Observable('installed')
        self.uninstalled = Observable('uninstalled')

        required_fields = ['ADDON_NAME', 'ADDON_SCOPES', 'BASE_URL', 'APP_NAME']
        super().__init__(source_file, config, required_config_fields=required_fields, *args, **kwargs)

        self.key = self.config.get('ADDON_KEY') or self.config.get('APP_NAME')
        self.scopes = self.config.get('ADDON_SCOPES', '').replace(',', ' ').split()
        self.allow_rooms = self.config.get('ADDON_ALLOW_ROOM', False)
        self.allow_globals = self.config.get('ADDON_ALLOW_GLOBAL', False)

        self.router.add_route('GET', '/', self.get_descriptor)

        self.router.add_route('POST', INSTALLABLE_PATH, self.on_install)
        self.router.add_route('POST', INSTALLABLE_PATH + '/{base64_data}', self.on_install)
        self.router.add_route('DELETE', INSTALLABLE_PATH + '/{oauth_id}', self.on_uninstall)
        self.router.add_route('DELETE', INSTALLABLE_PATH + '/{base64_data}/{oauth_id}', self.on_uninstall)

        self.authenticator = JwtAuthenticator(self)

    def add_capability_description(self, capability, description):
        """
        Add the given description to our list of known capabilities (e.g. web hook, webpanel)

        This is an internal method
        """
        _logger.debug('Registering capability %s: %s', capability, description)
        self.capabilities_descriptions.setdefault(capability, []).append(description)

    @asyncio.coroutine
    def load_oauth(self, oauth_id):
        """
        Returns the authorized client with the given oauth id, if one has been defined.
        For addons, the authorized client is more or less synonymous with a single installation
        """
        data = yield from self.store.get(oauth_id)
        return OAuth.from_map(data, self.cache) if data else None

    @asyncio.coroutine
    def on_install(self, request):
        """
        Install this addon according to the given request
        """
        from .toolkit import retry_json  # allows mocking in tests

        data = yield from request.json()
        _logger.info('Request to install: %s', dict_without(data, 'oauthSecret'))

        # We've received a request to install this addon. Ensure that the request is sensible
        capabilities = yield from retry_json(data.get('capabilitiesUrl'))
        err = self._validate_install_request(data, capabilities)
        if err:
            return _invalid_install(err)

        # Decide which scopes the installation needs. The installation URL may have encoded
        # scopes into the url if scopes are being dynamically generated
        install_scopes = self._get_scopes()
        base64_data = request.match_info.get('base64_data')
        if base64_data:
            install_scopes = self._decode_scopes_from_data(base64_data, install_scopes)

        # Create a authorized client that represents this installation of the addon
        oauth = OAuth(data['oauthId'],
                      data['oauthSecret'],
                      room_id=data.get('roomId'),
                      capdoc=capabilities,
                      scopes=install_scopes,
                      cache=self.cache)
        # The final piece of information that the installation needs is the
        # group into which the install was done. There isn't an obvious way to calculate this.
        # THINK - why isn't that information included in the installation data?
        # noinspection PyBroadException
        try:
            yield from oauth._fill_group_details()
        except:
            return _invalid_install('Unable to retrieve token using the new OAuth information')

        # Once we know all the information about the installation, we allow the addon
        # to restrict itself to particular groups (useful if you want to limit the addon
        # to just your own company)
        if self.group_validator:
            err = self.group_validator(int(oauth.group_id))
            if err:
                return _invalid_install(err)

        # This is the key step of the install.
        # This says that this addon has been installed for a particular oauth_id
        yield from self.store.set(oauth.id, oauth.to_map())
        _logger.info('Created installation: %s', dict_without(oauth.to_map(), 'oauthSecret'))

        yield from self.installed.fire({'oauth': oauth})

        return web.HTTPCreated()

    def _validate_install_request(self, data, capabilities):
        """
        Ensure that the install request is reasonable and complete.

        :param data: json from the install request
        :param capabilities: json of the capabilities of addon
        :return: An error if one is found or None if everything is fine
        """
        if not ('oauthId' in data and 'oauthSecret' in data and 'capabilitiesUrl' in data):
            return 'Install request is missing required data'

        room_id = data.get('roomId')
        if not room_id and not self.allow_globals:
            return ('This add-on can only be installed in individual rooms. '
                    "Please visit the 'Add-ons' link in a room's administration area and install from there.")

        if room_id and not self.allow_rooms:
            return ('This add-on cannot be installed in an individual room. '
                    "Please visit the 'Add-ons' tab in the 'Group Admin' area and install from there.")

        capabilities_url = data['capabilitiesUrl']
        link_to_self = nested_get(capabilities, 'links', 'self')
        if link_to_self != capabilities_url:
            return "The capabilities URL '%s' doesn't match the resource's self link '%s'" % \
                   (capabilities_url, link_to_self)

    @asyncio.coroutine
    def on_uninstall(self, request):
        """
        Handle a request to uninstall a particular installation of this addon.

        The request url includes the oauth id of the installation.

        Unfortunately, at this point, the oauth token is no longer valid in HipChat so the addon
        cannot actually do anything in HipChat. It can only clean up its own state.

        :param request:
        :return:
        """
        oauth_id = request.match_info['oauth_id']
        oauth = yield from self.load_oauth(oauth_id)
        if oauth:
            yield from self.store.delete(oauth.id)
            yield from self.uninstalled.fire({'oauth': oauth})

        return web.HTTPNoContent()

    @asyncio.coroutine
    def get_descriptor(self, request):
        """
        Create and return a full descriptor of this addon.

        For reference: https://www.hipchat.com/docs/apiv2/capabilities

        :param request:
        :return: A web response, containing the descriptor in plain text
        """
        descriptor = yield from self._generate_descriptor()

        descriptor_json = json.dumps(descriptor)
        _logger.info('Capabilities descriptor: %s', descriptor_json)

        return web.Response(text=descriptor_json)

    @asyncio.coroutine
    def _generate_descriptor(self):
        """
        Create a full descriptor for this add on
        :return:
        """

        config = self.config
        name = config.get('ADDON_NAME')
        scopes = self._get_scopes()
        callback_url = self._generate_installable_url(scopes)
        avatar_url = self.maybe_relative_to_base(config.get('ADDON_AVATAR'))
        avatar2x_url = self.maybe_relative_to_base(config.get('ADDON_AVATAR_2X'))

        descriptor = {
            'key': config.get('ADDON_KEY'),
            'name': name,
            'description': config.get('ADDON_DESCRIPTION', ''),
            'links': {
                'self': config.get('BASE_URL')
            },
            'capabilities': {
                'installable': {
                    'allowRoom': self.allow_rooms,
                    'allowGlobal': self.allow_globals,
                    'callbackUrl': callback_url
                },
                'hipchatApiConsumer': {
                    'fromName': config.get('ADDON_FROM_NAME', name),
                    'scopes': scopes,
                    'avatar': {
                        'url': avatar_url,
                        'url@2x': avatar2x_url
                    },
                }
            },
            'vendor': {
                'url': config.get('ADDON_VENDOR_URL', ''),
                'name': config.get('ADDON_VENDOR_NAME', '')
            }
        }

        # Add features that are registered via method decorators
        for event, descriptors in self.capabilities_descriptions.items():
            descriptor['capabilities'][event] = descriptors

        return descriptor

    def _get_scopes(self):
        """
        Return the list of scopes that this addon requires to execute

        See 'Scopes' section of https://www.hipchat.com/docs/apiv2/auth
        """
        if self.scopes_getter:
            return self.scopes_getter()

        return self.scopes

    def _generate_installable_url(self, scopes):

        # Preserve the scopes as they stood at installation time.
        # This is only important if the scopes can be dynamically generated
        installable_data = {
            'scopes': scopes
        }

        encoded = json.dumps(installable_data).encode('utf-8')
        base64_installable_data = base64.urlsafe_b64encode(encoded).decode('utf-8')
        return self.relative_to_base(INSTALLABLE_PATH + '/' + base64_installable_data)

    def _decode_scopes_from_data(self, base64_data, default_scopes):
        """
        Try to decode and return scopes from base64 data created by the above routine.
        If we can't decode the scopes, return default_scopes instead.
        """
        try:
            decoded_data = base64.urlsafe_b64decode(base64_data.encode('utf-8'))
        except binascii.Error:
            _logger.error("Could not decode scopes from '%s'", base64_data)
            return default_scopes

        json_data = json.loads(decoded_data.decode('utf-8'))
        return json_data.get('scopes', default_scopes)



