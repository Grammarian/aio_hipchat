import asyncio
import logging
import sys

from datetime import datetime
from logging.handlers import SysLogHandler
from urllib.parse import urlparse

import aio_local
import aiohttp_debugtoolbar
import aiohttp_jinja2
import asyncio_redis
import jinja2
import os
from aiohttp import web

from .execution_guard import ExecutionGuard
from .log import RequestIdLoggingFilter
from .cache import RedisCache, InMemoryCache
from .message_hub import RedisMessageHub, InMemoryMessageHub
from .store import MongoDbStore, InMemoryStore
from .toolkit import MillisecondTimer, simple_response, Observable

_logger = logging.getLogger(__name__)
_default_log_format = (
    '[%(asctime)s.%(msecs)03d] [%(process)d:%(threadName)s] [%(request_path)s#%(request_id)s] '
    '[%(name)s] [%(levelname)s] %(message)s'
)
_default_log_date_format = '%m-%d %H:%M:%S'


class WebApp(web.Application):
    """
    Instances of WebApp provide the main loop and primary routing mechanism for a web application.

    The actual functionality of the webapp should be provided by a subclass of Api.

    This class initializes logging, jinja, mongodb and redis (if so configured)

    config can be either a dictionary (in which case it will be used exactly as is without any changes)
    or it can be a callable in which can it should return the dictionary of config values (factory pattern).
    """

    def __init__(self, source_file, config, required_config_fields=None, *args, **kwargs):

        # Raiseable events
        self.started = Observable('started')

        self.config = self._init_config(source_file, config)
        debug = self.config.get('DEBUG', False)  # self.debug isn't available until after super().__init__ is called
        self.init_logging(debug)
        self.task_local = aio_local.local()

        self._log_start()

        self.validate_config(self.config, required_config_fields)

        # Make sure 'middlewares' contains a modifiable list
        middlewares = list(kwargs.get('middlewares', ()))
        if debug:
            kwargs['debug'] = True
            middlewares.append(aiohttp_debugtoolbar.middleware)

        guard = ExecutionGuard(_logger)
        middlewares.append(guard.middleware)

        kwargs['middlewares'] = middlewares

        super().__init__(*args, **kwargs)

        if debug:
            aiohttp_debugtoolbar.setup(self, enabled=True)
            self.loop.set_debug(True)

        self.init_jinja()

        self.mongo = self.init_mongo()
        self.redis = self.init_redis()

        self.store = self.init_store(self.mongo)
        self.cache = self.init_cache(self.redis)
        self.message_hub = self.init_message_hub()

        if self.config.get('USE_STANDARD_HANDLERS'):
            self._register_standard_handlers()

        self.loop.create_task(self.started.fire())

    def _log_start(self):
        """
        Log that the server has started
        """
        _logger.info('=====================================')
        _logger.info("Starting: %s", self.config.get('SOURCE_FILE'))
        _logger.info("Application: %s", self.config.get('APP_NAME'))
        _logger.info('Configuration:')
        for k, v in sorted(self.config.items()):
            _logger.info('  %s: %s', k, v)
        _logger.info('=====================================')

    def _register_standard_handlers(self):
        """
        Register the standard event handlers
        """
        self.router.add_route('GET', '/healthcheck', lambda x: simple_response('ok'), name="healthcheck")
        self.router.add_route('GET', '/localtime', lambda x: simple_response(str(datetime.now())), name="localtime")

    def _init_config(self, source_file, config):
        """
        Initialize our configuration from the given values. Return a dictionary of config values

        :param source_file:
        :param config: A dictionary or a callable which returns a dictionary
        :return: A dictionary of config values
        """
        config_dict = config() if callable(config) else config

        config_dict['SOURCE_FILE'] = source_file
        config_dict['BASE_URL'] = config_dict.get('BASE_URL', '').rstrip('\\')  # THINK - why rstrip on backslash?

        return config_dict

    def init_logging(self, debug):
        """
        Do all the initialization required to setup logging according to our configuration.

        :param debug:
        """

        log_level = logging.DEBUG if debug else logging.INFO

        # Create the handlers that match our config settings
        handlers = []
        if self.config.get('LOG_TO_FILE'):
            handlers.append(logging.FileHandler(self.config['LOG_FILE_NAME'], 'a'))

        if self.config.get('LOG_TO_SYSLOG'):
            handlers.append(SysLogHandler(facility=SysLogHandler.LOG_LOCAL3, address=self.config.get('LOG_TO_SYSLOG')))

        if self.config.get('LOG_TO_STDOUT'):
            handlers.append(logging.StreamHandler(sys.stdout))

        # All logging is turned off! That can't be a good idea
        # We can't write an log messages here, since we are still configuring it.
        warn_about_logging = not handlers
        if warn_about_logging:
            log_level = logging.ERROR
            handlers.append(logging.StreamHandler(sys.stdout))

        log_format = self.config.get('LOG_FORMAT') or _default_log_format
        log_date_format = self.config.get('LOG_DATE_FORMAT') or _default_log_date_format

        logging.basicConfig(
            level=log_level,
            format=log_format,
            datefmt=log_date_format,
            handlers=handlers
        )
        for x in handlers:
            x.addFilter(RequestIdLoggingFilter(self))

        # If we turned on emergency logging, warn about it
        if warn_about_logging:
            logging.error('All logging was disabled. Error level logging to stdout has been enabled')

        # The asyncio produces lots of output on DEBUG level, so let's not do that
        aio_log = logging.getLogger('asyncio')
        if debug:
            # aio_log.setLevel(logging.INFO)  # actually, even INFO is fairly verbose. Uncomment if you want to see
            aio_log.setLevel(logging.WARN)
            aio_log.propagate = True
        else:
            aio_log.setLevel(logging.WARN)

    def validate_config(self, config, required_fields=None):
        """
        Ensure that the given config is sensible. Raise a ValueError if it isn't
        """
        for required in (required_fields or ['APP_NAME', 'BASE_URL']):
            if not config.get(required):
                _logger.critical('Missing required config value: ' + required)
                raise ValueError('Missing required config value: ' + required)

    def init_jinja(self):
        dir_name = os.path.dirname(self.config['SOURCE_FILE'])

        static_path = os.path.join(dir_name, self.config['STATIC_FILES_DIR'])
        _logger.info('Static path => %s', static_path)
        self.router.add_static('/static', static_path, name='static')

        views_path = os.path.join(dir_name, self.config['JINJA_VIEWS_DIR'])
        _logger.info('Views path => %s', views_path)
        aiohttp_jinja2.setup(self, autoescape=True, loader=jinja2.FileSystemLoader(views_path))

    def init_mongo(self):
        url = self.config.get('MONGO_URL')
        if not url:
            _logger.info('MongoDb not configured. Using InMemoryStore')
            return None

        _logger.info('MongoDB => %s', url)

        from motor.motor_asyncio import AsyncIOMotorClient
        timer = MillisecondTimer()
        c = AsyncIOMotorClient(host=url, max_pool_size=self.config.get('MONGO_POOL_SIZE'))
        result = c.get_default_database()
        _logger.debug('... connected to mongo (in %d ms) => %s', timer.end(), result)

        return result

    def init_redis(self):
        redis_url = self.config.get('REDIS_URL')
        if not redis_url:
            _logger.info('Redis not configured. Using InMemoryCache')
            return None

        _logger.info('Redis => %s', redis_url)

        # A db index can be included in the url: https://somewhere.com:7689/2/
        url = urlparse(redis_url)
        db = 0
        try:
            if url.path:
                db = int(url.path.replace('/', ''))
        except (AttributeError, ValueError):
            pass

        timer = MillisecondTimer()
        result = self.loop.run_until_complete(asyncio_redis.Pool.create(
                host=url.hostname, port=url.port, password=url.password,
                db=db, poolsize=self.config.get('REDIS_POOL_SIZE')))
        _logger.debug('... connected to redis (in %d ms) => %s', timer.end(), result)

        return result

    def init_store(self, mongo):
        """
        Create a store either using the given mongo connection or an in memory one.
        """
        store = MongoDbStore(mongo) if mongo else InMemoryStore()
        return store

    def init_cache(self, redis):
        """
        Create a cache either using the given redis connection or an in memory one.
        """
        cache = RedisCache(redis) if redis else InMemoryCache()
        return cache

    def init_message_hub(self):
        """
        Create a message hub to allow service instances to communicate with each other.

        If you want *different* applications to communicate with each other, you will have
        to create your own instance of RedisMessageHub with a common namespace
        """
        namespace = self.config.get('MESSAGE_HUB_NAMESPACE') or self.app_name

        timer = MillisecondTimer()
        hub = RedisMessageHub.create(self.loop, self.redis, namespace) if self.redis else InMemoryMessageHub()
        _logger.info("Message hub (connected in %d ms) => %s", timer.end(), hub)

        return hub

    def add_route(self, http_method, path, handler, route_name, expect_handler):
        """
        Add a given route for this application. If the path ends with a slash, the handler will also
        respond to the path minus the trailing slash.

        Use the more convenient Api.route() rather than calling this method directly.
        """
        self.router.add_route(http_method, path, handler, name=route_name, expect_handler=expect_handler)

        # If the path has a trailing slash, also match a path without that slash
        if path != '/' and path.endswith('/'):
            self.router.add_route(http_method, path[:-1], handler, expect_handler=expect_handler)

    @property
    def app_name(self):
        """
        Return the name of the application. This will not be none or empty.
        """
        return self.config.get('APP_NAME')

    @property
    def base_url(self):
        """
        Return the base url for the application. This will not be none or empty.
        """
        return self.config.get('BASE_URL')

    def maybe_relative_to_base(self, partial_path):
        """
        Returns the given path as relative to our base url, unless the path is an
        absolute url.
        """
        return partial_path if '://' in partial_path else self.relative_to_base(partial_path)

    def relative_to_base(self, partial_path):
        """
        Return the given path relative to our base url
        """
        base = self.base_url.rstrip('/')
        partial_path = partial_path if partial_path.startswith('/') else '/' + partial_path
        return base + partial_path
