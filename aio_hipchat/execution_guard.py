import asyncio
from aiohttp import web
import traceback
from aiohttp.web_exceptions import HTTPException

from .toolkit import MillisecondTimer, simple_response, Observable, generate_random_string


class ExecutionGuard:
    """
    Guard the execution of each request with a wrapper that can log the execution of each request
    as well as catching and reporting unhandled exceptions
    """

    def __init__(self, logger):
        self._logger = logger

    @asyncio.coroutine
    def middleware(self, app, handler):

        @asyncio.coroutine
        def inner(request, *args, **kwargs):
            return (yield from self._guard(app, handler, request, *args, **kwargs))

        return inner

    @asyncio.coroutine
    def _guard(self, app, handler, request, *args, **kwargs):
        try:
            # Setup these task local values so they can be used in logging
            app.task_local.request_id = request.headers.get('X-REQUEST-ID') or ("REQ" + generate_random_string())
            app.task_local.request_path = request.path

            self._logger.debug("[ENTER] %s", handler.__name__)
            result = yield from handler(request, *args, **kwargs)
            self._logger.debug("[EXIT] %s", handler.__name__)

            return result
        except HTTPException:
            raise
        except Exception as ex:
            self._logger.exception("Handler failed: " + str(ex) + traceback.format_exc().replace('\n', '\\n'))
            return web.HTTPInternalServerError(reason=str(ex))
