import jwt
import logging
import asyncio

from .toolkit import nested_get
from .cache import InMemoryCache

_log = logging.getLogger(__name__)

ACCESS_TOKEN_CACHE = 'hipchat-tokens:{oauth_id}:{scopes}'


class OAuth:
    """
    Instances of this class represent an entity that can be uniquiely identified and that
    have permission (scopes) to take actions in a room or a group.

    When an addon is installed into a room (or globally into a group), an new oauth
    client is created. This information has to be persisted since it cannot be recovered.

    Every action on an addon (except install) will begin by creating one of these.
    """
    def __init__(self, id, secret=None, homepage=None, capabilities_url=None, room_id=None, token_url=None,
                 group_id=None, group_name=None, capdoc=None, scopes=None, cache=None):
        self.id = id
        self.room_id = room_id
        self.secret = secret
        self.group_id = group_id
        self.group_name = group_name or None
        self.homepage = nested_get(capdoc, 'links', 'homepage') or homepage or None
        self.token_url = nested_get(capdoc, 'capabilities', 'oauth2Provider', 'tokenUrl') or token_url or None
        self.capabilities_url = nested_get(capdoc, 'links', 'self') or capabilities_url or None
        self.scopes = tuple(scopes or [])
        self.cache = cache or InMemoryCache()

        self._room_client = None

    def to_map(self):
        """
        Return a dictionary that persists the state of the object.

        NB: Owing to the way from_map() works, the key names must match the parameter
        names in the constructor
        """
        return {
            'id': self.id,
            'secret': self.secret,
            'room_id': self.room_id,
            'group_id': self.group_id,
            'group_name': self.group_name,
            'homepage': self.homepage,
            'token_url': self.token_url,
            'capabilities_url': self.capabilities_url,
            'scopes': ','.join(self.scopes)
        }

    @staticmethod
    def from_map(data, cache):
        """
        Create a new instance of this class using data that was created via to_map().
        """
        # THINK - Where do the underscore keys come from?
        filtered = {k: v for k, v in data.items() if not k.startswith('_')}
        filtered['scopes'] = filtered.get('scopes', '').split(',')
        return OAuth(cache=cache, **filtered)

    @property
    def id_query(self):
        return {'id': self.id}

    @property
    def api_base_url(self):
        return self.capabilities_url[0:self.capabilities_url.rfind('/')]

    def has_scope(self, scope):
        return scope in self.scopes

    @asyncio.coroutine
    def get_token(self, scopes=None):
        """
        Retrieve a short lived authorization token that can be used by this installation to
        do the given kinds of operations (scopes).
        
        The token will be taken from the cache if possible. If not, a new one will be generated.
        """
        if scopes is None:
            scopes = self.scopes

        cache_key = ACCESS_TOKEN_CACHE.format(oauth_id=self.id, scopes=','.join(scopes))

        token = yield from self.cache.get(cache_key)

        # If the token wasn't in the cache, fetch a new one and store it.
        # Tokens have only a limited life, so we set a time-to-live on the cached value
        if not token:
            data = yield from self.generate_token(scopes)
            token = data['access_token']
            yield from self.cache.setex(key=cache_key, value=token, seconds=data['expires_in'] - 20)
        return token

    @asyncio.coroutine
    def generate_token(self, scopes=None):
        """
        Request a new auth token from the hipchat server. Also returns some other useful info.

        See https://www.hipchat.com/docs/apiv2/method/generate_token
        """
        from .toolkit import retry_json_ex  # allows mocking in tests

        flat_scopes = ' '.join(scopes or self.scopes)
        _log.debug('Requesting token from %s for %s', self.token_url, flat_scopes)

        status, json, raw = yield from retry_json_ex('POST',
                                                     self.token_url,
                                                     data={'grant_type': 'client_credentials', 'scope': flat_scopes},
                                                     auth=(self.id, self.secret))
        if status == 200:
            return json

        if status == 401:
            _log.error('Client %s is invalid', self.id)
            raise OauthClientInvalidError(self)

        raise Exception('Invalid token response: %s, %s', status, raw)

    def sign_jwt(self, user_id, data=None):
        """
        Encode the given data with our identification and secret
        """
        if data is None:
            data = {}
        data['iss'] = self.id,
        data['prn'] = user_id
        return jwt.encode(data, self.secret)

    @asyncio.coroutine
    def _fill_group_details(self):
        """
        When an addon is installed, it doesn't know the group into which it is being installed.
        (THINK: is that true? I think the group id is known, but not the group name)
        This method extracts the group details from a generated token.
        """
        session = yield from self.generate_token()
        self.group_id = session['group_id']
        self.group_name = session['group_name']


class OauthClientInvalidError(Exception):
    def __init__(self, client, *args, **kwargs):
        super(OauthClientInvalidError, self).__init__(*args, **kwargs)
        self.client = client
