import asyncio
import logging
import functools
import sys
from aiohttp import web

from .log import logged
from .api import Api
from .toolkit import dict_without_empty_values, add_allow_cross_origin, maybe_coroutine, nested_get, simple_response
from .hc_structures import json_value
_logger = logging.getLogger(__name__)

_KNOWN_HOOKS = ('room_archived', 'room_created', 'room_deleted', 'room_enter', 'room_exit', 'room_file_upload',
                'room_notification', 'room_topic_change', 'room_unarchived')
_KNOWN_AUTH = ('none', 'jwt')

class HcAddonApi(Api):
    """
    Base class for all hipchat addon api implementations.

    To implement a hipchat addon, you should subclass this and use its
    static methods to decorate the implementation of the various facilities
    your addon provides.

    This class provides static methods to register an api's functionality,
    and instance methods to execute that registered functionality.
    """

    @property
    def authenticator(self):
        return self.app.authenticator

    @staticmethod
    def message_hook(pattern, name, path=None, auth="jwt", extract=True):
        """
        Mark a method to be invoked whenever a message matching the given pattern is sent to a room.

        See 'webhook' method for the menaning of 'auth' and 'extract'
        """
        if auth not in _KNOWN_AUTH:
            raise ValueError("Unknown auth: {}. Must be one of: {}".format(auth, _KNOWN_AUTH))

        if not path:
            path = '/event/room_message/' + (name or 'default')

        args = {
            'type': 'webhook',
            'event': 'room_message',
            'pattern': pattern,
            'auth': auth,
            'extract': extract
        }

        def inner(f):
            Api._mark_func_as_api(f, path, 'POST', name, args=args)
            return f

        return inner

    @staticmethod
    def webhook(event, name=None, path=None, auth="jwt", extract=True):
        """
        Mark a method to be invoked whenever the given event occurs in HipChat.

        If given, 'path' is the url path through which this hook will be called.
        It must be unique across your api. If not given, a path will be constructed
        from the event and name

        Name allows your app to retrieve the route to this handler by name.

        'auth' should be left as 'jwt' (the default). This provides a high confidence that the
        message did come from hipchat itself, and is not from a third party.

        If 'extract' is true, the request will be prepopulated (if possible) with the following attributes:
          * body_json
          * oauth
          * oauth_client_id
          * message
          * room
          * sender

        If 'extract' is true and the extraction does not succeed, a InternalError will be returned
        and the handler will not be called
        """
        if event not in _KNOWN_HOOKS:
            raise ValueError("Unknown webhook event: '{}'. Must be one of: {}".format(event, _KNOWN_HOOKS))
        if auth not in _KNOWN_AUTH:
            raise ValueError("Unknown auth: '{}'. Must be one of: {}".format(auth, _KNOWN_AUTH))

        if not path:
            path = '/event/' + event + ('/' + name if name else '')

        args = {
            'type': 'webhook',
            'event': event,
            'auth': auth,
            'extract': extract
        }

        def inner(f):
            Api._mark_func_as_api(f, path, 'POST', name, args=args)
            return f

        return inner

    @staticmethod
    def webpanel(key, label, name=None, icon='', icon_hdpi=None, location="hipchat.sidebar.right", path=None):
        if not path:
            path = "/webpanel/" + key

        args = {
            'type': 'webpanel',
            'key': key,
            'label': label,
            'icon': icon,
            'icon_hdpi': icon_hdpi,
            'location': location
        }

        def inner(f):
            Api._mark_func_as_api(f, path, 'GET', name, args=args)
            return f

        return inner

    @staticmethod
    def glance(key, label, name=None, icon='', icon_hdpi=None, path=None, target=None, conditions=None):
        if not path:
            path = "/glance/" + key

        if not target:
            target = "%s.sidebar" % key

        if not name:
            name = key

        args = {
            'type': 'glance',
            'key': key,
            'label': label,
            'icon': icon,
            'icon_hdpi': icon_hdpi,
            'target': target,
        }
        if conditions:
            args.update(json_value(conditions))

        def inner(f):
            Api._mark_func_as_api(f, path, 'GET', name, args=args)
            return f

        return inner

    def _register_api(self, method_name, method, path, http_methods, route_name, expect_handler, args):
        """
        Register the given method for the given path in our app.
        """

        # Check if this method has been registered as a special type of api which needs further processing
        api_type = args.get('type') if args else None
        if api_type == 'webhook':
            decorated_method = self._register_webhook(method, path, route_name, args)
        elif api_type == 'webpanel':
            decorated_method = self._register_webpanel(method, path, route_name, args)
        elif api_type == 'glance':
            decorated_method = self._register_glance(method, path, route_name, args)
        else:
            decorated_method = method

        super()._register_api(method_name, decorated_method, path, http_methods, route_name, expect_handler, args)

    def _register_webhook(self, method, path, route_name, args):

        event = args['event']
        auth = args['auth']
        pattern = args.get('pattern')
        extract = args.get('extract', False)

        webhook_capability = {
            "event": event,
            "url": self.app.relative_to_base(path),
            "name": route_name,
            "pattern": pattern,
            "authentication": auth
        }

        self.app.add_capability_description('webhook', dict_without_empty_values(webhook_capability))
        return self._wrap_method(method, require_auth=(auth == 'jwt'), allow_cors=True, extract=extract)

    def _register_webpanel(self, method, path, route_name, args):

        key = args['key']
        label = args['label']
        location = args['location']
        icon = args['icon']
        icon_hdpi = args['icon_hdpi'] or icon

        webpanel_capability = {
            "key": key,
            "name": {
                "value": label
            },
            "url": self.app.relative_to_base(path),
            "location": location,
            "icon": {
                "url": self.app.maybe_relative_to_base(icon),
                "url@2x": self.app.maybe_relative_to_base(icon_hdpi)
            },
        }

        self.app.add_capability_description('webPanel', webpanel_capability)
        return self._wrap_method(method, require_auth=True, allow_cors=False)

    def _register_glance(self, method, path, route_name, args):

        key = args['key']
        label = args['label']
        icon = args['icon']
        icon_hdpi = args['icon_hdpi'] or icon
        target = args['target']
        conditions = args.get('conditions')

        glance_capability = {
            "key": key,
            "name": {
                "value": label
            },
            "icon": {
                "url": self.app.maybe_relative_to_base(icon),
                "url@2x": self.app.maybe_relative_to_base(icon_hdpi)
            },
            "queryUrl": self.app.relative_to_base(path),
            "target": target,
            'conditions': conditions
        }

        self.app.add_capability_description('glance', dict_without_empty_values(glance_capability))
        return self._wrap_method(method, require_auth=True, allow_cors=True)

    def _wrap_method(self, method, require_auth, allow_cors, extract=False):
        @logged()
        @asyncio.coroutine
        @functools.wraps(method)
        def inner(request):

            if require_auth:
                has_auth = yield from self._check_auth(request)
                if not has_auth:
                    _logger.debug("failed auth check")
                    return web.HTTPUnauthorized(text="Unauthorized request, please check the JWT token")

            if extract:
                try:
                    yield from self._extract_details(request)
                except:
                    _logger.exception("Extract json failed")
                    return web.HTTPInternalServerError()

            request.theme = request.GET.get("theme", "light")
            result = yield from maybe_coroutine(method, request)
            resp = simple_response(result)

            if allow_cors:
                add_allow_cross_origin(resp)

            return resp

        return inner

    @asyncio.coroutine
    def _check_auth(self, request):
        _logger.debug("Validating jwt")
        oauth, data, signed_request = yield from self.authenticator.validate(request)
        if oauth:
            request.oauth = oauth
            request.oauth_client_id = oauth.id
            request.jwt_data = data
            request.signed_request = signed_request
            request.token = self.authenticator.create_session_token(self.app.key, oauth, data)

        return oauth is not None

    @asyncio.coroutine
    def _extract_details(self, request):
        """
        Try to convert the body of the request into json, and then extract some useful details,
        which are stored back into the request.
        """
        request.body_as_json = yield from request.json()

        # It's possible that these fields have already been populated by the auth check
        if 'oauth_client_id' not in request:
            request.oauth_client_id = request.body_as_json.get('oauth_client_id')
            if request.oauth_client_id:
                request.oauth = yield from self.app.load_oauth(request.oauth_client_id)

        request.message = nested_get(request.body_as_json, "item", "message")
        request.room = nested_get(request.body_as_json, "item", "room")
        request.sender = nested_get(request.body_as_json, "item", "sender")
