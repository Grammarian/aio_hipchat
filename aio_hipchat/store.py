import asyncio
from abc import abstractmethod, ABC


class AbstractStore(ABC):

    @abstractmethod
    @asyncio.coroutine
    def get(self, key):
        pass

    @abstractmethod
    @asyncio.coroutine
    def set(self, key, value):
        pass

    @abstractmethod
    @asyncio.coroutine
    def delete(self, key):
        pass


class InMemoryStore(AbstractStore):
    
    def __init__(self):
        self.store = {}
        
    @asyncio.coroutine
    def get(self, key):
        return self.store.get(key, None)

    @asyncio.coroutine
    def set(self, key, value):
        self.store[key] = value

    @asyncio.coroutine
    def delete(self, key):
        del self.store[key]


class MongoDbStore(AbstractStore):  # pragma: no cover

    def __init__(self, mongodb, table_name="clients"):
        self.mongodb = mongodb
        self.table_name = table_name

    @asyncio.coroutine
    def set(self, key, value):
        table = self.mongodb[self.table_name]
        yield from table.remove(self.id_query(key))
        yield from table.insert(value)

    @asyncio.coroutine
    def get(self, key):
        table = self.mongodb[self.table_name]
        return (yield from table.find_one(self.id_query(key)))

    @asyncio.coroutine
    def delete(self, key):
        table = self.mongodb[self.table_name]
        yield from table.remove(self.id_query(key))

    def id_query(self, key):
        return {"id": key}

