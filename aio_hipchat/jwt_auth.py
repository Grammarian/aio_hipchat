import asyncio
import datetime

from calendar import timegm
from abc import abstractmethod, ABC

import jwt
import logging

_log = logging.getLogger(__name__)


class Authenticator(ABC):

    @abstractmethod
    @asyncio.coroutine
    def validate(self, request):
        pass

    @abstractmethod
    def create_session_token(self, addon_key, oauth, jwt_data):
        pass


class JwtAuthenticator(Authenticator):

    JWT_LEEWAY_IN_SECONDS = 60 * 5
    TOKEN_EXPIRY_IN_MINUTES = 15

    def __init__(self, app):
        self.app = app

    @asyncio.coroutine
    def validate(self, request):
        signed_request = request.GET.get('signed_request', None) or \
                         request.headers.get("x-acpt") or \
                         request.headers.get("authorization")

        if not signed_request:
            return None, None, None

        # If the signing has a JWT prefix, remove it
        if signed_request.startswith("JWT "):
            signed_request = signed_request[4:]

        unverified_claims = jwt.decode(signed_request, verify=False)
        oauth_id = unverified_claims['iss']

        # THINK -- I suspect something horribly subtle is happening with this oauth_id dance :(

        # The audience claim identifies the intended recipient, according to the JWT spec,
        # but we still allow the issuer to be used if 'aud' is missing.
        # Session JWTs make use of this (the issuer is the add-on in this case)
        audience = unverified_claims.get('aud')
        if audience is not None:
            oauth_id = audience[0]

        oauth = yield from self.app.load_oauth(oauth_id)
        if not oauth:
            return None, None, None

        data = jwt.decode(signed_request,
                          oauth.secret,
                          options={"verify_aud": False},
                          leeway=self.JWT_LEEWAY_IN_SECONDS)
        return oauth, data, signed_request

    def create_session_token(self, addon_key, oauth, jwt_data, utc_now=None):
        """
        Create a token that can be used to validate request done within this session

        The structure is that the addon is the issuer of the token and a particular
        installation (the oauth) is the audience.

        :param addon_key:
        :param oauth:
        :param jwt_data:
        :param utc_now:
        :return:
        """
        now_utc = utc_now or datetime.datetime.utcnow()
        exp = now_utc + datetime.timedelta(minutes=self.TOKEN_EXPIRY_IN_MINUTES)
        payload = {
            'iss': addon_key,
            'sub': jwt_data['sub'],
            'iat': timegm(now_utc.utctimetuple()),
            'exp': timegm(exp.utctimetuple()),
            'aud': [oauth.id],
            'context': jwt_data['context']
        }

        return jwt.encode(payload, oauth.secret).decode("utf-8")
