import asyncio
import json
import logging
import signal
import weakref

from abc import abstractmethod, ABC
from aiohttp.web import StreamResponse
from collections import namedtuple
from concurrent.futures import CancelledError


class AbstractPusher(ABC):

    @abstractmethod
    @asyncio.coroutine
    def push(self, data, event_name=None, event_id=None):
        pass

    @abstractmethod
    def stop(self):
        pass


class MultiPusher(AbstractPusher):
    """
    Instances of this class allow one server to push SSE notifications to all
    connected clients.

    To do this, this class publishes data notifications to a central message hub
    and forward received messages to all connected sse connections.

    Web services that are hosted in responsive data centres can be running multiple
    copies of itself at any time, as the hosting server decides to spin up more
    (or less) instances to cope with demand.

    To receive SSE events, clients connect to a webserver, but these connections
    are spread across the available services. So, each service has only a portion of all
    sse connections at any one time.

    When any single webservice wants to send a message to ALL sse connections,
    it must first send the message to all other instances of itself, and then
    each instance pushes out the message to the sse connections that it knows about.

    That is what this class does
    """

    @staticmethod
    def create(app, out_pusher, topic=None):
        """
        Create a live multi-pusher that pushes to all clients connected to any server
        """
        multi_pusher = MultiPusher(app.message_hub, out_pusher, topic)
        app.loop.run_until_complete(multi_pusher.start())
        return multi_pusher

    @staticmethod
    def createSse(app, path, topic=None, ping_interval=0):
        """
        Create a live multi-pusher that can send SSE to all clients connected to any server
        """
        sse_pusher = SsePusher(app, path, ping_interval)
        return MultiPusher.create(app, sse_pusher, topic)

    def __init__(self, message_hub, pusher, topic=None):
        self.message_hub = message_hub
        self.pusher = pusher
        self.topic = topic or 'multi-pusher'
        self._logger = logging.getLogger('MultiPusher:' + self.topic)

    @asyncio.coroutine
    def start(self):
        self._logger.debug('starting')
        yield from self.message_hub.subscribe(self.topic, self._handle_incoming)

    @asyncio.coroutine
    def push(self, data, event_name=None, event_id=None):
        event = {
            'data': data,
            'event_name': event_name,
            'event_id':event_id
        }
        yield from self.message_hub.publish(self.topic, event)

    def stop(self):
        self.pusher.stop()

    def _handle_incoming(self, incoming_data):
        self._logger.debug('_handle_incoming: %r', incoming_data)
        self.pusher.push(
            data=incoming_data.get('data'),
            event_name=incoming_data.get('event_name'),
            event_id=incoming_data.get('event_id')
        )


class SsePusher(AbstractPusher):
    """
    Push json structures out over server-sent events.

    When messages are sent, they can be given a sequence identifier.
    When a client reconnects, it can pass back the identifier of the last
    message it received. The pusher will then try to resend missed messages.

    Regardless of the above recovery, the pusher will always remember the
    last pushed event, and send that immediately to newly connected clients.
    This is helpful for feeds that occur only intermittently.

    Messages can be pushed with an event_type as well. This makes partitioning
    of incoming messages easier on the client.

    A pusher can be configured to periodically send 'ping' messages to subscribers.

    Useful resources:
    http://cjihrig.com/blog/the-server-side-of-server-sent-events/
    http://www.howopensource.com/2014/12/introduction-to-server-sent-events/
    http://taoofmac.com/space/blog/2014/11/16/1940

    Reference:
    http://www.w3.org/TR/2011/WD-eventsource-20110208/
    """

    # Tell our clients to retry connections after this period - default value
    RECONNECT_INTERVAL_IN_MILLISECONDS = 5 * 1000

    # If we push this many messages to a client without them being processed, drop that client
    MAX_BUFFER = 5

    _time_to_die_sentinel = object()

    def __init__(self, app, path, ping_interval=0, reconnect_interval=RECONNECT_INTERVAL_IN_MILLISECONDS):
        """
        Create an object that will listen on the given endpoint for the given app
        and allow data to be pushed via Server Side Events to clients that listen on that
        endpoint

        :param app: a web.Application hosting the server side event
        :param path: string indicating the endpoint to which clients will connect
        :param ping_interval: integer indicates the seconds between ping messages to the clients
        """
        self.app = app
        self.last_event = None
        self.ping_interval = ping_interval
        self.reconnect_interval = reconnect_interval
        self.pinger = None
        self._logger = logging.getLogger(__name__ + path)

        # If we keep only a weak reference to the queue that joins this pusher to the stream response then
        # when the stream response goes away (normally when the client disconnects), that
        # whole network of objects will be gc'ed. Which gives us a very elegant way to
        # transparently discard disconnected clients -- they literally just disappear!
        self.subscribers = weakref.WeakSet()

        self.app.router.add_route('GET', path, self._handle_subscription)

        # Shut down on SIGINT
        try:
            self.app.loop.add_signal_handler(signal.SIGINT, self.stop)
        except RuntimeError:
            pass  # Windows doesn't support signal handlers

    def push(self, data, event_name=None, event_id=None, cache=True):
        """
        Send the given data out to all subscribers.

        If cache is true, the data event will be remembered and supplied immediately to new subscribers.

        :param data a dictionary or other object that can be converted to json
        :param event_name, string, indicating the type of even
        :param event_id, number or string, indicating the sequence id of this push.
        :param cache: bool, should this event be remembered and forwarded to new client.
        """
        sse_event = _SseEvent(json.dumps(data), event_name, event_id)
        if cache:
            self.last_event = sse_event

        # If no one is listening to the event, we don't need to do anything else
        if not self.subscribers:
            return

        self._logger.debug("Pushing to %d subscribers: %s", len(self.subscribers), sse_event.data)
        for q in list(self.subscribers):
            try:
                q.put_nowait(sse_event)
            except asyncio.QueueFull:
                self._logger.debug('Dead client connection. Cleaning up')
                self._cleanup_connection(q)

    def stop(self):
        """
        Shutdown this pusher, closing all open connections
        """
        self._logger.debug("Closing down %d subscribers", len(self.subscribers))

        if self.pinger:
            self.pinger.stop()

        for q in list(self.subscribers):
            self._cleanup_connection(q)

    def _cleanup_connection(self, q):
        """
        The connection processing the given q has not processed several messages.
        Declare it dead and clean it up.

        This is also used when the pusher is force stopped.

        This situation only arises in unusual circumstances (e.g. when a browser actually hangs).
        Normal disconnections don't come through this route.
        """
        while not q.empty():
            q.get_nowait()
        q.put_nowait(self._time_to_die_sentinel)
        self.subscribers.remove(q)

    @asyncio.coroutine
    def _handle_subscription(self, request):
        """
        Handle an incoming request to subscribe to server-side events
        """
        self._logger.debug("Received SSE subscription: %r", request)

        # Start the pinger if we need one and don't have one
        if self.ping_interval and not self.pinger:
            self.pinger = _Pinger(self, self.app.loop, self.ping_interval)

        stream_response = StreamResponse(
            headers={
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/event-stream',
                "Connection": "keep-alive",

                # allow cors
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET",
                "Access-Control-Allow-Headers": "Content-Type",
            }
        )

        # Streams have to be prepared for a particular request
        yield from stream_response.prepare(request)

        # Is this a resync attempt?
        last_message_id = request.headers.get('Last-Event-ID')
        resynced = self._resync(stream_response, last_message_id) if last_message_id else False

        # If we haven't done a specific resync and have a previous event for this pusher, send it immediately
        if self.last_event and not resynced:
            self._push_one_event(stream_response, self.last_event)

        q = asyncio.Queue(self.MAX_BUFFER)
        self.subscribers.add(q)

        yield from self._run_loop(q, stream_response)

        return stream_response

    @asyncio.coroutine
    def _run_loop(self, q, stream_response):
        while True:
            try:
                sse_event = yield from q.get()
            except CancelledError:
                # Happens when subscriber disconnects
                self._logger.debug('Connection to subscriber closed')
                return

            if sse_event == self._time_to_die_sentinel:
                self._logger.debug("Received time-to-die signal")
                yield from stream_response.write_eof()
                stream_response.force_close()
                return

            self._push_one_event(stream_response, sse_event)

    def _push_one_event(self, stream_response, sse_event):
        """
        Low level write a correctly formatted byte stream to the given response.
        """
        flat_event = sse_event.flatten(self.reconnect_interval)
        stream_response.write(flat_event.encode('utf-8'))

    def _resync(self, stream_response, last_message_id):
        """
        Resend all messages from the given message id until current.

        Return True if the resync succeeded, False otherwise
        """
        self._logger.debug("Resync from message: %s", last_message_id)

        # TO DO

        return False


# noinspection PyClassHasNoInit
class _SseEvent(namedtuple('SseEvent', ['data', 'event_name', 'event_id'])):
    """
    Simple data holder for SS event data
    """

    def flatten(self, retry):
        """
        Translate the given bits of information into the format required for SSE
        """
        items = [
            ("event", self.event_name),
            ("id", self.event_id),
            ("data", self.data),
            ("retry", retry),
        ]
        lines = ["%s: %s" % (field, value) for field, value in items if value]
        flat = "\n".join(lines) + "\n\n"
        return flat


class WsPusher(AbstractPusher):
    """
    Push events out to WebSocket listeners

    TO DO
    """

    def push(self, data, event_name=None, event_id=None, cache=True):
        pass

    def stop(self):
        pass


class _Pinger:
    """
    Simple class that periodically sends ping messages to all subscribers
    """

    def __init__(self, pusher, loop, interval):
        self.pusher = pusher
        self.loop = loop
        self.interval = interval
        self.loop.call_soon(self._ping)
        self.running = True

    def _ping(self):
        if not self.running:
            return

        self.pusher.push({}, "ping", cache=False)
        self.loop.call_later(self.interval, self._ping)

    def stop(self):
        self.running = False

if __name__ == '__main__':
    import sys
    import itertools
    from datetime import datetime
    from aiohttp.web import Application

    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    app = Application()
    pusher = SsePusher(app, "/subscribe")
    count = itertools.count()

    def push_updates(loop):
        pusher.push({
            "now": str(datetime.now())
        }, "update", next(count))
        loop.call_later(15, push_updates, loop)

    loop = asyncio.get_event_loop()
    handler = app.make_handler()
    f = loop.create_server(handler, '0.0.0.0', 28865)
    srv = loop.run_until_complete(f)
    print('serving on', srv.sockets[0].getsockname())

    loop.call_soon(push_updates, loop)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(handler.finish_connections(1.0))
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(app.finish())
    loop.close()
