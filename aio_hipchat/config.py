
# Copy app_config into your app and modify as appropriate.
# If there are environment variables with the same name, the value of the
# environment variable will be used at runtime, overriding the value set here

app_config = {
    'DEBUG': True,
    'ADDON_KEY': '',
    'ADDON_NAME': '',
    'ADDON_VENDOR_NAME': '',
    'ADDON_VENDOR_URL': '',
    'ADDON_FROM_NAME': '',
    'ADDON_DESCRIPTION': '',
    'ADDON_AVATAR': '',
    'ADDON_AVATAR_2X': '',
    'ADDON_ALLOW_ROOM': 'true',
    'ADDON_ALLOW_GLOBAL': 'false',
    'ADDON_SCOPES': 'scope1 scopes2',  # space-separated list of scopes required by the add on

    'APP_NAME': '',
    'BASE_URL': '',

    # Remove this if you don't use redis
    'REDIS_URL': 'redis://localhost:6379',

    # Remove this if you don't use mongo
    'MONGO_URL': 'mongodb://localhost:27017/test',
}

# You can change these things through app_config or environment variables, but you normally won't have to
default_config = {
    'MONGO_POOL_SIZE': 2,
    'REDIS_POOL_SIZE': 2,

    # The url to a MongoDB instance is normally controlled via MONGO_URL config setting.
    # Some MongoDb providers use a different environment variable to store that url.
    # If you are using such a provider, then set MONGO_ALTERNATIVE_ENV_VAR in your config (or environment)
    # to be the name of the env var that holds the connection URL.
    'MONGO_URL_ALTERNATE_ENV_VAR': '',

    # As above, but for Redis
    # In fact, any config setting can be redirected this, but these are the most common
    'REDIS_URL_ALTERNATE_ENV_VAR': '',

    # Should log lines be sent to stdout? (boolean)
    'LOG_TO_STDOUT': 'true',

    # If you want to log to a file, set this to a valid path.
    'LOG_TO_FILE': '',

    # If you aren't logging to a file, set this to send logging to syslog.
    # This should be set to something like: '/var/run/syslog' (if on a mac) or '/dev/log'
    'LOG_TO_SYSLOG': '',

    # What format should be used when writing to the logs? For production, you may want a shorter form.
    # If not explicitly set, the format will be:
    # '[%(asctime)s.%(msecs)03d] [%(process)d:%(threadName)s] [%(request_path)s#%(request_id)s] '
    # '[%(name)s] [%(levelname)s] %(message)s'
    'LOG_FORMAT': '',

    # When date are written to the log, what format should be used?
    # Defaults to '%m-%d %H:%M:%S'
    'LOG_DATE_FORMAT': '',

    # Messages passed to and from the hub will use this value as prefix so that different applications
    # don't interfere with each other. If this is not set, the application name will be used
    'MESSAGE_HUB_NAMESPACE': '',
}

# You can change these things ONLY through app_config (no environment vars)
code_only_config = {
    'STATIC_FILES_DIR': 'static',
    'JINJA_VIEWS_DIR': 'views',
    'USE_STANDARD_HANDLERS': True,
}

# These config entries will have their values converted to booleans.
# The values True, 1, '1', 'true', 'True' or 'TRUE' will be converted to True. All other values are false.
# If you add your own config values and you want them to be handled as booleans, import this value
# and then add your own config names to the list.
boolean_config_values = [
    'DEBUG',
    'ADDON_ALLOW_ROOM',
    'ADDON_ALLOW_GLOBAL',
    'LOG_TO_STDOUT'
]
