import asyncio
from datetime import datetime, timedelta
from abc import abstractmethod, ABC


class AbstractCache(ABC):

    @abstractmethod
    @asyncio.coroutine
    def get(self, key):
        pass

    @abstractmethod
    @asyncio.coroutine
    def delete(self, key):
        pass

    @abstractmethod
    @asyncio.coroutine
    def set(self, key, value):
        pass

    @abstractmethod
    @asyncio.coroutine
    def setex(self, key, seconds, value):
        pass

    @abstractmethod
    @asyncio.coroutine
    def exists(self, key):
        pass

    @abstractmethod
    @asyncio.coroutine
    def incr(self, key, by=1):
        pass

    @abstractmethod
    @asyncio.coroutine
    def decr(self, key, by=1):
        pass

    @abstractmethod
    @asyncio.coroutine
    def sadd(self, key, values):
        pass

    @abstractmethod
    @asyncio.coroutine
    def srem(self, key, value):
        pass

    @abstractmethod
    @asyncio.coroutine
    def smembers(self, key):
        pass

    @abstractmethod
    @asyncio.coroutine
    def expire(self, key, seconds):
        pass

    @asyncio.coroutine
    def lpush(self, key, values):
        pass

    @asyncio.coroutine
    def lrange(self, key, from_index=0, to_index=-1):
        pass


class RedisCache(AbstractCache):  # pragma: no cover

    def __init__(self, redis):
        super().__init__()
        self.redis = redis

    @asyncio.coroutine
    def get(self, key):
        return (yield from self.redis.get(key))

    @asyncio.coroutine
    def set(self, key, value):
        return (yield from self.redis.set(key, value))

    @asyncio.coroutine
    def setex(self, key, seconds, value):
        return (yield from self.redis.setex(key, seconds, value))

    @asyncio.coroutine
    def delete(self, key):
        return (yield from self.redis.delete([key]))

    @asyncio.coroutine
    def incr(self, key, by=1):
        if by == 1:
            result = yield from self.redis.incr(key)
        else:
            result = yield from self.redis.incrby(key, by)
        return result

    @asyncio.coroutine
    def decr(self, key, by=1):
        if by == 1:
            result = yield from self.redis.decr(key)
        else:
            result = yield from self.redis.decrby(key, by)
        return result

    @asyncio.coroutine
    def sadd(self, key, values):
        return (yield from self.redis.sadd(key, values))

    @asyncio.coroutine
    def exists(self, key):
        return (yield from self.redis.exists(key))

    @asyncio.coroutine
    def expire(self, key, seconds):
        return (yield from self.redis.expire(key, seconds))

    @asyncio.coroutine
    def smembers(self, key):
        result = yield from self.redis.smembers(key)
        members = yield from result.asset()
        return members

    @asyncio.coroutine
    def srem(self, key, values):
        return (yield from self.redis.srem(key, values))

    @asyncio.coroutine
    def lpush(self, key, values):
        if isinstance(values, str):
            values = [values]
        return (yield from self.redis.lpush(key, values))

    @asyncio.coroutine
    def lrange(self, key, from_index=0, to_index=-1):
        result = yield from self.redis.lrange(key, from_index, to_index)
        members = yield from result.aslist()
        return members


class InMemoryCache(AbstractCache):

    def __init__(self):
        super().__init__()
        self.cache = {}
        self.expiries = {}

    @asyncio.coroutine
    def set(self, key, value):
        self.cache[key] = value

    @asyncio.coroutine
    def setex(self, key, seconds, value):
        self.cache[key] = value
        yield from self.expire(key, seconds)

    @asyncio.coroutine
    def get(self, key):
        cached_value = self.cache.get(key)
        if cached_value is not None:
            if self._is_expired(key):
                del self.cache[key]
                del self.expiries[key]
            else:
                return cached_value

        return None

    @asyncio.coroutine
    def delete(self, key):
        self.cache.pop(key, None)

    @asyncio.coroutine
    def incr(self, key, by=1):
        value = yield from self.get(key)
        value = int(value or 0) + by
        yield from self.set(key, value)
        return value

    @asyncio.coroutine
    def decr(self, key, by=1):
        return (yield from self.incr(key, 0 - by))

    @asyncio.coroutine
    def exists(self, key):
        return key in self.cache and not self._is_expired(key)

    @asyncio.coroutine
    def sadd(self, key, values):
        s = self.cache.setdefault(key, set())
        diff = set(values).difference(s)
        s.update(diff)
        return len(diff)

    @asyncio.coroutine
    def srem(self, key, values):
        s = self.cache.setdefault(key, set())
        intersection = s.intersection(set(values))
        if intersection:
            s.difference_update(intersection)
        return len(intersection)

    @asyncio.coroutine
    def smembers(self, key):
        s = self.cache.get(key)
        return s if s is not None else set()

    @asyncio.coroutine
    def expire(self, key, seconds):
        self.expiries[key] = datetime.now() + timedelta(seconds=seconds)

    def _is_expired(self, key):
        expires = self.expiries.get(key)
        return datetime.now() >= expires if expires else False

    @asyncio.coroutine
    def lpush(self, key, values):
        lst = self.cache.setdefault(key, list())
        for x in reversed(values):
            lst.insert(0, x)

    @asyncio.coroutine
    def lrange(self, key, from_index=0, to_index=-1):
        lst = self.cache.setdefault(key, list())
        return lst[from_index:] if to_index == -1 else lst[from_index:to_index]

if __name__ == '__main__':
    import sys
    import logging
    import asyncio_redis
    from pprint import pprint

    log_format = '[%(asctime)s] [%(process)d] [%(name)s] %(message)s'

    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format=log_format)

    loop = asyncio.get_event_loop()

    @asyncio.coroutine
    def run_test():
        x = yield from asyncio_redis.Pool.create(host='localhost', port=6379)
        cache = RedisCache(x)
        lst = yield from cache.lrange('chatter:history:1')
        pprint(lst)

    t = loop.create_task(run_test())

    try:
        loop.run_until_complete(t)
    except KeyboardInterrupt:
        pass
    loop.close()
