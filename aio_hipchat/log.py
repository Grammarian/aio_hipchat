import asyncio
from aiohttp.web import Request
from functools import wraps
import logging
import inspect
from contextlib import contextmanager, ExitStack

from .toolkit import maybe_coroutine, MillisecondTimer

_default_logger = logging.getLogger(__name__)


def make_default_logger(name_or_logger):
    """
    Create a logger that will used be by default for all function logging

    :param name_or_logger: an existing logger or the name that should be used to create one
    :return:
    """
    global _default_logger
    _default_logger = logging.getLogger(name_or_logger) if isinstance(name_or_logger, str) else name_or_logger
    return _default_logger


def logged(logger=None, dump_args=True, first_arg_is_self=False):
    """
    Log the start and end of a function or coroutine, optionally logging all args too.

    This produces output when the given logger is at DEBUG level. At any other level, this does nothing.

    Call make_default_logger() if you don't want to pass a logger to each call.

    Usage:

    @logged()
    def some_function(parm1, parm2):
        pass

    The braces are important. @logged() works, @logged does not

    :param logger: Logger used to trace function entry and exit. If None, uses default logger
    :param dump_args: If this is true, the value of all parameters will be written to the log
    :param first_arg_is_self: If dumping args, don't dump the first arg if this is true.
    :return: A decorated function
    """

    @contextmanager
    def _log_execution(func, *args, **kwargs):
        log = logger if logger else _default_logger

        # If we're not debugging, we don't need to do anything else
        if not log.isEnabledFor(logging.DEBUG):
            yield
            return

        if dump_args:
            first_arg_to_dump = 1 if first_arg_is_self else 0
            args_as_strings = [repr(a) for a in args[first_arg_to_dump:]]
            kwargs_as_strings = ["%s=%s" % (k, repr(v)) for k, v in sorted(kwargs.items())]
            all_args = ', '.join(args_as_strings + kwargs_as_strings)
        else:
            all_args = ''

        timer = MillisecondTimer()
        log.debug(">>> %s(%s)", func.__name__, all_args)
        yield
        log.debug("<<< %s (in %d ms)", func.__name__, timer.end())

    def actual_decorator(func):
        if inspect.isgeneratorfunction(func):
            @asyncio.coroutine
            @wraps(func)
            def inner(*args, **kwargs):
                with _log_execution(func, *args, **kwargs):
                    result = yield from maybe_coroutine(func, *args, **kwargs)
                    return result
        else:
            @wraps(func)
            def inner(*args, **kwargs):
                with _log_execution(func, *args, **kwargs):
                    return func(*args, **kwargs)

        return inner

    return actual_decorator


def mlogged(logger=None, dump_args=True):
    """
    Log the execution of an instance method.

    Parameters are as for logged()
    """
    return logged(logger, dump_args, first_arg_is_self=True)

def dump(obj, logger=None):
    """
    Dump all the properties of the given object to the given logger.

    DO NOT USE THIS IN PRODUCTION CODE
    """
    log = logger if logger else _default_logger
    import pprint
    log.debug(pprint.pformat(vars(obj)))


class RequestIdLoggingFilter(logging.Filter):
    """
    A filter that adds the request_id and request_path attributes to the record, if available.
    """
    def __init__(self, app, name=''):
        self.app = app
        super().__init__(name)

    def filter(self, record):
        try:
            task_local = self.app.task_local
            record.request_id = task_local.request_id if task_local.request_id else ''
            record.request_path = task_local.request_path if task_local.request_path else ''
        except (AttributeError, ValueError):
            # likely not initialized yet
            record.request_id = ''
            record.request_path = ''
        return True

__all__ = ["dump", "logged", "make_default_logger", "RequestIdLoggingFilter"]
