import uuid

"""
This module contains structures for major DTO that are used by HipChat.
At the moment, the focus is on structures that support HipChat addons,
including the various forms of card notifications.

"""


def json_value(raw):
    """
    Convert the given value into a JSON compatible dictionary.

    If the given value is a container, each element in the container
    will also be converted to json, recursively.

    :param raw:
    :return: A JSON dictionary
    """
    if hasattr(raw, 'as_json'):
        return raw.as_json()
    if isinstance(raw, dict):
        return {k: json_value(v) for k,v in raw.items()}
    if isinstance(raw, (list, set, tuple)):
        return [json_value(x) for x in raw]
    return raw


def default_encode(o):
    """
    Use this as the 'default' parameter to json.dumps() to serialize HcStructures

    Usage:
       str = json.dumps(some_data, default=default_encode)

    :param o: the object to serialise. json doesn't know how to encode it.
    :return:
    """
    if hasattr(o, 'as_json'):
        return o.as_json()
    else:
        raise TypeError(repr(o) + " is not JSON serializable")


class HcStructure:
    """
    A HcStructure represents a JSON structure used within HC

    Each subclass should specify which __slots__ it uses.

    In general, HcStructures are constructed by passing keyword arguments to the constructor.
    Some subclasses provide convienence constructors.

    It can optionally specify "_validations", a dictionary which maps slot name to a
    list of values that are acceptable for that slot
    """
    __slots__ = []

    _validations = {}

    def __init__(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)

    @staticmethod
    def _json_name(slot_name):
        """
        For a given slot name, return the property name that should be used in json.

        Some json properties used by HipChat have names that are not valid python identifiers
        The general strategy is to just append an underscore to the identifier. So, the
        'from' property has a slot named 'from_'.

        There is nothing we can do with the property called 'url@2x', so just call it 'url_2x'
        and change it here
        """
        if slot_name.endswith('_'):
            return slot_name[:-1]

        if slot_name == 'url_2x':
            return 'url@2x'
        return slot_name

    def as_json(self):
        return {
            HcStructure._json_name(x): json_value(getattr(self, x))
            for x in self.__slots__
            if getattr(self, x, None) is not None
        }

    def __setattr__(self, key, value):
        valid_values = self.__class__._validations.get(key)
        if valid_values and not value in valid_values:
            raise ValueError("'{}' is not a valid value for '{}'. Must be: {}".format(value, key, valid_values))
        super().__setattr__( key, value)

    def __str__(self):
        return '{}({})'.format(self.__class__.__name__, self.as_json())


class Icon(HcStructure):
    __slots__ = 'url', 'url_2x'

    def __init__(self, url, url_2x=None):
        self.url = url
        # Some commands allow url@2x to be empty -- others do not, so let's just always provide it
        self.url_2x = url_2x or url


class GlanceContainer(HcStructure):
    __slots__ = ["glance"]

    def __init__(self, glances):
        """
        glances is map of key => glance
        """
        self.glance = [_GlanceContainerItem(key=k, content=v) for k,v in glances.items()]


class _GlanceContainerItem(HcStructure):
    __slots__ = "key", "content"


class Glance(HcStructure):
    """
    Glance can have either an extra icon or a lozenge -- not both

    Glances can dynamically change their labels, and the lozenge or icon that appears to the right
    of the label.

    The image to the left of the label is taken from the addon resource description and cannot be altered.

    The 'metadata' parameter is a dict that maps attributes to values. These are used to match any condition
    that was given when the glance was registered -- this is used to determine if this glance is visible.
    """

    __slots__ = "label", "metadata", "status"

    def __init__(self, label, lozenge_text=None, lozenge_type=None, icon_url=None, icon_url_2x=None, metadata=None):
        self.label = {'type': 'html', 'value': label} if label else None
        self.metadata = metadata
        if lozenge_text:
            self.status = _GlanceStatus(
                type='lozenge',
                value=_GlanceLozenge(label=lozenge_text, type=lozenge_type or 'default'))
        elif icon_url:
            self.status = _GlanceStatus(type='icon', value=Icon(icon_url, icon_url_2x))


class _GlanceStatus(HcStructure):
    __slots__ = 'type', 'value'
    _validations = {
        'type': ['lozenge', 'icon']
    }


class _GlanceLozenge(HcStructure):
    __slots__ = 'type', 'label'
    _validations = {
        'type': ['default', 'success', 'error', 'current', 'new', 'moved']
    }


class GlanceCondition(HcStructure):
    __slots__ = 'conditions', 'type'
    _validations = {
        'type': ['and', 'AND', 'or', 'OR']
    }

    def __init__(self, condition, params=None, and_=None, or_=None):
        if and_ and or_:
            raise ValueError("Only specify one of 'and_' or 'or_', not both")

        invert = condition and condition.startswith('not ')
        if invert:
            condition = condition[4:]

        self.conditions = [_GlanceSimpleCondition(condition, invert, params)]
        if and_ or or_:
            self.conditions.append(and_ or or_)
            self.type = 'and' if and_ else 'or'


class _GlanceSimpleCondition(HcStructure):
    __slots__ = 'condition', 'invert', 'params'
    _validations = {
        'condition': ['room_is_public', 'user_is_admin', 'user_is_guest', 'user_is_room_owner', 'glance_matches']
    }

    def __init__(self, condition, invert, params):
        self.condition = condition
        if invert:
            self.invert = invert
        if params:
            self.params = {'metadata': [params]}


class Thumbnail(HcStructure):
    __slots__ = 'url', 'url_2x', 'height', 'width'


class CardAttribute(HcStructure):
    __slots__ = 'label', 'value'

    def __init__(self, label, link_url=None, icon_url=None, icon_url_2x=None, second_label=None, second_label_style=None):
        self.label = label
        self.value = _CardAttributeValue(
            url=link_url, style=second_label_style, label=second_label, icon=Icon(icon_url, icon_url_2x)
        )


class _CardAttributeValue(HcStructure):
    __slots__ = 'url', 'style', 'label', 'icon'

    _validations = {
        'style': ['lozenge-success', 'lozenge-error', 'lozenge-current', 'lozenge-complete', 'lozenge-moved', 'lozenge'],
    }


class _Card(HcStructure):
    __slots__ = 'style', 'description', 'format', 'url', 'title', 'activity', 'attributes', 'id', 'thumbnail', 'icon'
    _validations = {
        'style': ['file', 'image', 'application', 'link', 'processed', 'media'],
        'format': ['compact', 'medium']
    }


class _CardDescription(HcStructure):
    __slots__ = 'value', 'format'
    _validations = {
        'format': ['html', 'text'],
    }

    def __init__(self, description, is_html):
        self.value = description
        self.format = 'html' if is_html else 'text'


class _CardActivity(HcStructure):
    __slots__ = 'html', 'icon'


class ImageCard(_Card):
    """
    Image cards have a click-able image, above a click-able title, above an optional description (which can be html).

    If 'side_by_side' is true, the image will be to the left of the title and description.

    'width' and 'height' don't seem to do anything
    """
    def __init__(self, image_url, title=None, description=None, description_is_html=False,
                 link_url=None, image_url_2x=None, width=None, height=None, id=None, side_by_side=False):
        self.style = 'media' if side_by_side else 'image'
        self.id = id or str(uuid.uuid4())
        self.url = link_url
        self.title = title
        if description:
            self.description = description
            #self.description = _CardDescription(description, description_is_html)
        self.thumbnail = Thumbnail(url=image_url, url_2x=image_url_2x, width=width, height=height)


class LinkCard(_Card):
    """
    Link cards have a icon and click-able title, above an optional description (which can be html).
    """
    def __init__(self, title=None, link_url=None, description=None, description_is_html=False,
                 icon_url=None, icon_url_2x=None, id=None):
        self.style = 'link'
        self.id = id or str(uuid.uuid4())
        self.url = link_url
        self.title = title
        if description:
            self.description = description
            #self.description = _CardDescription(description, description_is_html)
        if icon_url or icon_url_2x:
            self.icon = Icon(icon_url, icon_url_2x)


class ApplicationCard(_Card):
    """
    Application cards are the most complicated.

    They have a icon and click-able title, above an optional description (which can be html).

    'attributes' (if given) should be a collection of CardAttributes, each of which is rendered as
    [label] [icon] [second label], where second label is a lozenge of various types.

    If a summary title or icon is given, that will be shown a single row [icon] [title] which can
    be expanded to show the entire card.

    Finally, these cards can be made slightly larger by setting 'format' to 'medium'.
    """
    def __init__(self, title=None, link_url=None, description=None, description_is_html=False,
                 icon_url=None, icon_url_2x=None, format='compact', attributes=None,
                 summary_html=None, summary_icon=None, id=None):
        self.style = 'application'
        self.id = id or str(uuid.uuid4())
        self.url = link_url
        self.title = title
        self.format = format
        if description:
            self.description = description
            #self.description = _CardDescription(description, description_is_html)
        if icon_url or icon_url_2x:
            self.icon = Icon(icon_url, icon_url_2x)
        self.attributes = attributes
        if summary_html or summary_icon:
            self.activity = _CardActivity(html=summary_html, icon=summary_icon)


class Notification(HcStructure):
    """
    This structure represents a hipchat notification object.

    See https://www.hipchat.com/docs/apiv2/method/send_room_notification
    """

    __slots__ = 'message', 'message_format', 'from_', 'icon', 'color', 'attach_to', 'notify', 'card'

    _validations = {
        'message_format': ['html', 'text'],
        'color': ['yellow', 'green', 'red', 'purple', 'gray', 'random']
    }


class CardNotification(HcStructure):
    """
    This structure represents a hipchat card notification object.

    See https://www.hipchat.com/docs/apiv2/method/send_room_notification.

    For cards, only the 'card' property takes effect, BUT 'message' is still required even if it is ignored
    """

    __slots__ = 'message', 'card'

    def __init__(self, card):
        self.card = card
        self.message = 'ignored'


if __name__ == '__main__':
    n = Notification(
        attach_to='kjhsdlkfjhsdlk',
        from_='sending user',
        notify=True,
        card=ApplicationCard(
            style="link",
            format="medium",
            description='<b><i>important</i></b> description for card',
            description_is_html=True,
            url="linkforcard",
            title='title for card',
            summary_html='the collapsable summary',
            summary_icon='url to icon',
            attributes= [
                CardAttribute('whatever', link_url="link", icon_url="icon", second_label="inside loz", second_label_style='lozenge')
            ]
        )
    )
    print(n)
