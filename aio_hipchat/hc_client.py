import asyncio
import json
import logging

from .toolkit import retry_request, nested_get

_logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 10


class HcClient:
    """
    Allow some basic operations back to HC.

    This is not even close to being full featured. Use something like HypChat to do that.
    """

    def __init__(self, oauth, timeout=DEFAULT_TIMEOUT):
        self.oauth = oauth
        self.timeout = timeout

    def _room_base_url(self, room_id):
        return '{api_url}/room/{room_id}'.format(api_url=self.oauth.api_base_url, room_id=room_id)

    def _push_room_glance_url(self, room_id):
        return '{api_url}/addon/ui/room/{room_id}'.format(api_url=self.oauth.api_base_url, room_id=room_id)

    @asyncio.coroutine
    def send_room_notification(self, notification, room_id=None):
        """
        Send a notification to the given room.

        :param room_id:
        :param notification:
        :return:
        """
        url = self._room_base_url(room_id or self.oauth.room_id) + "/notification"
        yield from self._send_auth_request(url, notification, "Sending room notification")

    @asyncio.coroutine
    def push_room_glance(self, glance, room_id=None):
        """
        Push a glance to a room

        :param room_id:
        :param glance:
        """
        url = self._push_room_glance_url(room_id or self.oauth.room_id)
        yield from self._send_auth_request(url, glance, "Pushing room glance")

    @asyncio.coroutine
    def get_participants(self, room_id, expanded=True, batch_size=100, max_results=1000, batch_processor=None):
        """
        Get the participants of a room, with lots of options.

        If batch_processor is given, it will be given a chance to examine each batch of users. If it returns False,
        no more participants will be fetched and the method will terminate.
        """
        headers = yield from self._auth_headers()
        url = "{base_url}/participant?max_results={page_size}{expanded}".format(
            base_url=self._room_base_url(room_id),
            page_size=batch_size,
            expanded="&expand=items" if expanded else "")

        items = list()
        while url is not None and len(items) < max_results:
            with (yield from retry_request('GET', url, headers=headers, timeout=self.timeout)) as resp:
                if resp.status == 200:
                    body = yield from resp.read(decode=True)
                    items_batch = body['items']
                    if batch_processor and batch_processor(items_batch) == False:
                        break

                    items.extend(items_batch)
                    url = nested_get(body, 'links', "next")
                else:
                    _logger.error("Cannot get participants for room %s. Resp=%s", url, resp.status)
                    return None
        return items

    @asyncio.coroutine
    def _auth_headers(self):
        """
        Create the headers required for an authorized request
        """
        token = yield from self.oauth.get_token()
        headers = {
            'content-type': 'application/json',
            'authorization': 'Bearer %s' % token
        }
        return headers

    @asyncio.coroutine
    def _send_auth_request(self, url, data, operation, method='POST', headers=None):
        """
        Send a request to a url without expecting any data back.

        Response of 200 or 204 are treated as success.

        :param url: The url to be invoked
        :param data: The JSON-able data to be sent
        :param operation: A text description of the action (for logging)
        :param method: Which HTTP operation will be used (GET, POST)
        :param headers: Headers for the request. Uses
        :return: Nothing
        """
        from .toolkit import retry_request

        data_as_json = data.as_json() if hasattr(data, "as_json") else data
        flat_data = json.dumps(data_as_json)

        _logger.info("%s to %s: %s", operation, url, flat_data)

        # Add authorization headers to request, merging with any given headers
        auth_headers = yield from self._auth_headers()
        if headers:
            headers.update(auth_headers)
        else:
            headers = auth_headers

        with (yield from retry_request(method, url, headers=headers, data=flat_data, timeout=self.timeout)) as resp:
            if resp.status not in (200, 204):
                body = yield from resp.read()
                _logger.error("%s failed: %s - %s", operation, resp.status, body)
