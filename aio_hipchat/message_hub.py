import asyncio
import json
import logging

from abc import ABC, abstractmethod
from aio_hipchat.toolkit import maybe_coroutine

_logger = logging.getLogger(__name__)

# Sigh -- 'ensure_future' hard replaced 'async' in 3.4.4 (a minor release!)
try:
    from asyncio import ensure_future
except ImportError:
    ensure_future = asyncio.async


class MessageHub(ABC):

    @abstractmethod
    @asyncio.coroutine
    def stop(self):
        pass

    @abstractmethod
    @asyncio.coroutine
    def subscribe(self, topic, handler):
        pass

    @abstractmethod
    @asyncio.coroutine
    def unsubscribe(self, topic, handler):
        pass

    @abstractmethod
    @asyncio.coroutine
    def publish(self, topic, data):
        pass


class InMemoryMessageHub(MessageHub):
    """
    Instances of this class act like a message hub, but only allow message passing within the application itself.
    """

    def __init__(self):
        self.subscriptions = {}

    @asyncio.coroutine
    def publish(self, topic, data):
        handlers = self.subscriptions.get(topic, [])
        tasks = [maybe_coroutine(x, data) for x in handlers]
        yield from asyncio.gather(*tasks)

    @asyncio.coroutine
    def unsubscribe(self, topic, handler):

        # Force this method to be a coroutine
        if False: yield

        existing = self.subscriptions.get(topic)
        if not existing:
            return

        try:
            existing.remove(handler)
        except ValueError:
            _logger.error("Tried to remove subscription handler from '%s', but the handler is unknown: %r",
                          topic, handler)
            return

    @asyncio.coroutine
    def subscribe(self, topic, handler):
        # Force this method to be a coroutine
        if False: yield
        existing = self.subscriptions.setdefault(topic, list())
        existing.append(handler)

    @asyncio.coroutine
    def stop(self):
        pass


class RedisMessageHub(MessageHub):
    """
    This class implements a simple message hub that allows processes that share the same
    Redis instance to communicate via publish/subscribe style message passing.

    Each message hub is given a 'namespace' parameter, which prefixes all publish/subscribe
    topics. This allows different messages hub clusters to exist on the same redis instance
    without their messages becoming intermingled. Usually the application name would be a
    useful namespace.

    Don't put wildcards in your namespace. Bad Things will happen.

    Useful links:
    http://redis.io/topics/pubsub
    """

    CHANNEL_FORMAT = 'message-hub:%s:'

    @staticmethod
    def create(loop, redis, namespace):
        """
        Create and return a pre-started message hub
        """
        hub = RedisMessageHub(redis, namespace)
        loop.run_until_complete(hub._start())
        return hub

    def __init__(self, redis, namespace):
        self.redis = redis
        self.namespace = self.CHANNEL_FORMAT % (namespace or 'default-ns')
        self.redis_subscription = None
        self.subscriptions = {}

    def __str__(self):
        return "{}(ns='{}', subscriptions={}, redis={})".format(
            self.__class__.__name__,
            self.namespace,
            len(self.subscriptions),
            self.redis
        )

    @asyncio.coroutine
    def _start(self):
        """
        Enable this message hub to send/receive messages
        """
        self.redis_subscription = yield from self.redis.start_subscribe()
        ensure_future(self._dispatcher())

    @asyncio.coroutine
    def stop(self):
        """
        Shutdown the message hub
        """
        if self.redis_subscription and self.subscriptions:
            pattern_subscriptions = [k for k in self.subscriptions if '*' in k]
            topic_subscriptions = [k for k in self.subscriptions if '*' not in k]
            yield from self.redis_subscription.punsubscribe(pattern_subscriptions)
            yield from self.redis_subscription.unsubscribe(topic_subscriptions)
        self.subscriptions = {}

    @asyncio.coroutine
    def subscribe(self, topic, handler):
        """
        Subscribe to the given topic and invoke the given handler
        when a message is published with a matching topic.

        :param topic: string, Can have wildcard characters to match multiple topics
        :param handler: a function or coroutine that accepts a single param, which is the published message
        """
        _logger.debug("Adding subscription '%s': %r", topic, handler)
        channel = self.namespace + topic
        existing = self.subscriptions.setdefault(channel, list())
        existing.append(handler)
        if len(existing) == 1:
            if '*' in topic:
                _logger.debug("Sending new pattern subscription to redis '%s'", channel)
                yield from self.redis_subscription.psubscribe([channel])
            else:
                _logger.debug("Sending new subscription to redis '%s'", channel)
                yield from self.redis_subscription.subscribe([channel])

    @asyncio.coroutine
    def unsubscribe(self, topic, handler):
        """
        Remove the given handler from the list of listeners to the given topic.

        If the given topic and/or handler has never been subscribed, no error is raised.
        """
        _logger.debug("Removing subscription '%s': %r", topic, handler)

        channel = self.namespace + topic
        existing = self.subscriptions.get(channel)
        if not existing:
            return

        try:
            existing.remove(handler)
        except ValueError:
            _logger.error("Tried to remove subscription handler from '%s', but the handler is unknown: %r",
                          channel, handler)
            return

        # If there are still other subscriptions, we don't need to do anything else
        if existing:
            return

        _logger.debug("Final subscription to '%s' removed. Unsubscribing from redis", channel)
        if '*' in topic:
            yield from self.redis_subscription.punsubscribe([channel])
        else:
            yield from self.redis_subscription.unsubscribe([channel])

    @asyncio.coroutine
    def publish(self, topic, data):
        """
        Publish the given json data onto the given topic.

        :param topic: a string, identifying the message. No wildcards!
        :param data: a dict or other structure convertible to a string via json.dumps
        """
        flat_data = json.dumps(data)
        _logger.debug("Publish to %s: %s", topic, flat_data)
        channel = self.namespace + topic
        yield from self.redis.publish(channel, flat_data)

    @asyncio.coroutine
    def _dispatcher(self):
        """
        Inner loop that receives subscription events and dispatches them to handlers
        """
        _logger.debug("Starting dispatcher")
        while True:
            result = yield from self.redis_subscription.next_published()

            # If the subscription was via a pattern, 'pattern' will be filled in
            # Otherwise, just dispatch on the
            yield from self._dispatch(result.pattern or result.channel, result.value)

    @asyncio.coroutine
    def _dispatch(self, topic, value):
        """
        Dispatch a received subscription event to matching handlers
        """
        _logger.debug("Dispatching %s with %s", topic, value)

        handlers = self.subscriptions.get(topic, [])
        if not handlers:
            return

        try:
            decoded_value = json.loads(value)
        except:
            decoded_value = value

        # Dispatch the handlers so they run in parallel
        tasks = [maybe_coroutine(x, decoded_value) for x in handlers]
        yield from asyncio.gather(*tasks)


if __name__ == '__main__':
    # This demo launches two subscriber processes, and publishes messages from this process
    import sys
    from datetime import datetime
    import asyncio_redis
    from urllib.parse import urlparse
    import multiprocessing

    log_format = '[%(asctime)s] [%(process)d] [%(name)s] %(message)s'

    logging.basicConfig(level=logging.INFO, stream=sys.stdout, format=log_format)

    loop = asyncio.get_event_loop()
    # loop.set_debug(True)
    redis_url = 'redis://localhost:6379'

    def subscriber(topic):

        logging.basicConfig(level=logging.INFO, stream=sys.stdout, format=log_format)

        loop2 = asyncio.get_event_loop()

        @asyncio.coroutine
        def subscriber_inner():
            _logger2 = logging.getLogger("SUBSCRIBER " + topic.upper())

            url = urlparse(redis_url)

            redis_pool = yield from asyncio_redis.Pool.create(
                host=url.hostname, port=url.port, password=url.password,
                db=0, poolsize=2)

            hub = RedisMessageHub(redis_pool, "inline-pubsub")
            yield from hub.start()

            yield from hub.subscribe(topic, lambda x: _logger2.info("received: {}".format(x)))

        loop2.create_task(subscriber_inner())
        loop2.run_forever()

    t = multiprocessing.Process(target=subscriber, args=('inline.topic',))
    t.daemon = True
    t.start()

    t2 = multiprocessing.Process(target=subscriber, args=('inline.*',))
    t2.daemon = True
    t2.start()

    @asyncio.coroutine
    def run_test():
        _logger3 = logging.getLogger("PUBLISHER")

        url = urlparse(redis_url)
        redis_pool = yield from asyncio_redis.Pool.create(
            host=url.hostname, port=url.port, password=url.password,
            db=0, poolsize=2)

        hub = RedisMessageHub(redis_pool, "inline-pubsub")
        yield from hub.start()

        while True:
            yield from asyncio.sleep(5)
            data = {"now": str(datetime.now())}
            _logger3.info("publishing: %r", data)
            yield from hub.publish('inline.topic', data)

    loop.create_task(run_test())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    loop.close()



