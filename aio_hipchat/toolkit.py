import json
import random
import string

import aiohttp
import asyncio
import logging
import contextlib
import functools
from time import time

_logger = logging.getLogger(__name__)


CONTENT_TYPE_JSON = 'application/json'
CONTENT_TYPE_TEXT = 'text/plain'

_uppercase_plus_digits = string.ascii_uppercase + string.digits


def generate_random_string(n=8):
    """
    Generate a string consisting of n random letters and numbers

    :type n: int
    :param n: The length of the desired string
    :return: A random series of uppercase and digits
    """
    return ''.join(random.choice(_uppercase_plus_digits) for _ in range(n))

@asyncio.coroutine
def maybe_coroutine(func, *args, **kwargs):
    """
    Invoke a function that may or may not be a coroutine.

    Inspired by Twisted`s t.i.defer.maybeDeferred
    """
    p = func(*args, **kwargs)
    if asyncio.iscoroutine(p):
        result = yield from p
    else:
        result = p
    return result


@asyncio.coroutine
def retry_request(method, url, timeout=10, backoffs=None, *args, **kwargs):
    """
    Try to get the given url several times.
    Returns a HTTPResponse wrapped in a disposable context.

    Usage:

    with (yield from retry_request('GET', url)) as resp:
        # do something interesting with resp

    # resp is closed and inaccessible
    """
    for back_off_period in (backoffs or (1, 4, 9)):
        try:
            t = MillisecondTimer()
            request = aiohttp.request(method, url, *args, **kwargs)
            res = yield from asyncio.wait_for(request, timeout)
            _logger.info("Called ({method}) {url} with result of {code} in {elapsed} ms".format(
                method=method, url=url, elapsed=t.end(), code=res.status
            ))
            return contextlib.closing(res)
        except TimeoutError:
            _logger.warn("Timeout calling {url}, attempt again in {sleep_for} seconds".format(
                url=url, sleep_for=back_off_period
            ))
            asyncio.sleep(back_off_period)

    raise TimeoutError()


@asyncio.coroutine
def retry_json(url, timeout=10, *args, **kwargs):
    """
    Fetch the given url and decode its response as json
    """
    status, json, raw = yield from retry_json_ex('GET', url, timeout, *args, **kwargs)
    return json


@asyncio.coroutine
def retry_json_ex(method, url, timeout=10, *args, **kwargs):
    """
    Fetch the given url and decode its response as json,
    Return the status, decoded json and raw content
    """
    _logger.debug("Retrieving json via %s: %s", method, url)

    json = {}
    status = 0
    raw = None
    if url:
        with (yield from retry_request(method, url, timeout, *args, **kwargs)) as resp:
            status = resp.status
            raw = yield from resp.read()
            if resp.status == 200:
                json = yield from resp.read(decode=True)

    _logger.debug("Retrieved %s => %s, %s", url, status, json)
    return status, json, raw


class MillisecondTimer:
    """
    Simple timer from creation until end()
    """
    def __init__(self):
        self.start = time()

    def end(self):
        """
        Returns the milliseconds since creation of this timer.
        Can be called multiple times and will give a different result each time.
        """
        elapsed = (time() - self.start) * 1000  # elapsed in ms
        return int(elapsed)


def allow_cross_origin(func):
    """
    A decorator that allows cross-origin resource sharing for everything and everyone.

    It can be applied to both sync functions and co-routines.
    """
    @functools.wraps(func)
    def inner(*args, **kwargs):
        response = yield from maybe_coroutine(func, *args, **kwargs)
        add_allow_cross_origin(response)
        return response

    return inner


def add_allow_cross_origin(response):
    """
    Mark the given response so that it allows cross-origin resource sharing for everything and everyone.
    """
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type")


def nested_get(d, *keys):
    """
    Fetched a nested element from a nested dictionary. Useful for json structures.
    Returns the resulting value after all keys have been de-referenced, or when
    the first non-dictionary in the chain of keys is encountered.

    For example:

    >>> nested_get({"A": {"B": 2, "C": {"D": 4, "E": 5}}}, "A", "C", "E")
    5
    >>> nested_get({"A": {"B": 2, "C": {"D": 4, "E": 5}}}, "A", "B", "E")
    2
    >>> nested_get({"A": {"B": 2, "C": {"D": 4, "E": 5}}}, "Z", "B", "E")

    """
    if not d:
        return None

    result = d
    for k in keys:
        result = result.get(k)
        if not isinstance(result, dict):
            return result
    return result


def dict_union(d1, d2):
    """
    Merge d2 into d1 and return a new dict. d1 and d2 are unmodified.

    Entries from d2 will overwrite entries with the same keys from d1

    In v3.5, this will replaced by d = {**d1, **d2}

    >>> dict_union({"A": 1, "B":2}, {"B": 3, "C": 4})
    {"A": 1, "B": 3, "C": 4}

    :param d1:
    :param d2:
    :return: Union of d1 and d2
    """
    if not d1:
        return d2.copy()
    dprime = d1.copy()
    dprime.update(d2)
    return dprime


def dict_without(d, *keys):
    """
    Return a copy of the given dictionary without the given keys.

    Useful for removing sensitive information before logging.

    >>> dict_without({"A": 1, "B":2, "C": 4}, "A", "C")
    {"B": 2}
    """
    return {k: v for k, v in d.items() if k not in keys}


def dict_without_empty_values(d):
    """
    Return a copy of the given dictionary without entries that
    have a null/empty value

    >>> dict_without_empty_values({"A": '', "B":2, "C": 0})
    {"B": 2}
    """
    return {k: v for k, v in d.items() if v}


def complete(result):
    """
    Create a completed Future with a given result. Useful for mocking results of async.coroutines

    Example:

    # get_user_id() is an async coroutine

    self.client = mock.Mock()
    self.client.get_user_id.return_value = complete("42")
    """
    f = asyncio.Future()
    f.set_result(result)
    return f


def simple_response(result, content_type=None):
    """
    Convert the given result into a web Response.

    Conversion is as follows:
    - An existing web.Response is returned unchanged.
    - None becomes HTTPNoContent.
    - strings become text responses.
    - dictionaries (and other structures) become JSON responses.

    If the given result has an 'as_json' attribute, that will be used to convert it
    to a JSON response.

    :param result:
    :param content_type:
    :return:
    """
    if isinstance(result, aiohttp.web.Response):
        return result

    if result is None:
        return aiohttp.web.HTTPNoContent()

    # If the given result as a specific way of representing itself as json, use that
    if hasattr(result, "as_json"):
        result = result.as_json()

    if isinstance(result, (dict, list, bool)):
        body = bytes(json.dumps(result).encode('utf-8'))
        ct = content_type or CONTENT_TYPE_JSON
    elif isinstance(result, str):
        body = bytes(result.encode('utf-8'))
        ct = content_type or CONTENT_TYPE_TEXT
    else:
        _logger.error("A web method failed because its result could not be converted to a http response: {!r}".format(result))
        return aiohttp.web_exceptions.HTTPNotImplemented()

    return aiohttp.web.Response(body=body, content_type=ct)


class Observable:
    """
    Simple observable implementation.

    This is more efficient than aiohttp's Signal, and not tied to an 'app',
    so it's usable everywhere.

    Usage:

    class MyInterestingClass:
        def __init__(self):
            self.installed = Observable('installed')
        def some_other_method(self):
            yield from self.installed.fire(param=1)

    class MyOtherClass:
        def __init__(self, interestingClass):
            interestingClass.installed.subscribe(self._event_listener)
        @asyncio.coroutine
        def _event_listener(self, params):
            print(params)
    """
    def __init__(self, event_name=None):
        self.event_name = event_name
        self.callbacks = []

    def subscribe(self, callback):
        """
        Register the given callback to be called when this observable is fired.

        :param callback: A callable function or async coroutine
        """
        self.callbacks.append(callback)

    def unsubscribe(self, callback):
        """
        Remove the given callback from our observers.

        This method will throw an exception if the given callback was not subscribed.

        :param callback: A callable function or async coroutine
        """
        self.callbacks.remove(callback)

    @asyncio.coroutine
    def fire(self, *args, **kwargs):
        """
        Invoke all our observers with the given parameters.

        You can pass any combination of arguments and keyword args, but for
        future proofing, passing a single dictionary is recommended.

        The observers are run in parallel if possible.
        """
        if not self.callbacks:
            return
        t = MillisecondTimer()
        tasks = [maybe_coroutine(x, *args, **kwargs) for x in self.callbacks]
        yield from asyncio.gather(*tasks)
        _logger.debug("Executing %d listeners for '%s' took %d ms", len(tasks), self.event_name, t.end())

if __name__ == "__main__":
    import doctest
    doctest.testmod()
