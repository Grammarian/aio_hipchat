aio_hipchat
===========

Building HipChat add-ons for fun and profit
-------------------------------------------

Have you noticed that all the cool kids are using HipChat?
The really cool kids are building their own extensions, using Atlassians' HipConnect
system: https://www.hipchat.com/docs/apiv2/addons.

So, rather than sitting there all by yourself, staring at the soggy sandwiches in
your lunchbox, you too can be a cool kid! You will be surrounded by cool froods,
swimming in money, and in the end, get the girl/guy/significant-other.
All you have to do to make your life complete, is write your own HipChat addon
[actual results may vary].

Complete success in life awaits only these three steps:

  1. Come up with a brilliant idea
  2. Implement it
  3. Use this toolkit to hook your brilliant idea implementation into HipChat.

Getting started
---------------

Clone (or download) this git repository as a starting point for your application.

https://Grammarian@bitbucket.org/Grammarian/aio_hipchat_getting_started.git

1. Edit app.py, changing the config settings as you wish
   APP_NAME

2. Edit app_api.py. Here is where you listen for events from HipChat, and hook them
   into your implementation. Have a quick look, but leave it as it is for the moment.

Heroku hosting
--------------

Once you have your implementation, you need somewhere for it to live. There are several
services that provide this. Here I will describe what is invoked to use the Heroku service.
