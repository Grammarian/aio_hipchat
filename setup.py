"""
aiolocals
---------
"""
from setuptools import setup

setup(
    name='aio_hipchat',
    version='0.0.6.dev0',
    url='https://bitbucket.org/grammariam/aio_hipchat',
    license='APLv2',
    author='HipChat',
    author_email='ppiper@atlassian.com',
    description="Foundation for building HpiChat addons as aiohttp services",
    long_description=__doc__,
    packages=['aio_hipchat'],
    include_package_data=True,
    platforms='any',
    install_requires=[
        "aiohttp",
        "aiohttp-jinja2",
        "jinja2",
        "aiohttp_debugtoolbar",
        "pyjwt",
        "motor",
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
